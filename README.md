# README #

Splinter project is a CMS written in PHP. 

*It is inspired by Wordpress and Drupal.
*It's architecture is based on the Model View Controller design pattern
*PHP native packages for routing,templating etc were forked from https://thephpleague.com/ 
*With Spliter you can build PHP websites fast, creating themes with dynamic fields,regions and sections

The source files of splinter CMS.

To build the dynamic templates of the project you need composer installed on your computer.
Open the command line and run the following commands in the root of the folder.

* `php composer.phar` (to install composer to your computer)
* `composer init` (to generate the composer.json file)

Development workflow:

* Run the gulp development server: `gulp watch`

References:

* [npm](https://www.npmjs.com/)
* [gulpjs](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
* [bower](http://bower.io/#getting-started) 
* [bower-installer](https://github.com/blittle/bower-installer) 
* [sass](http://sass-lang.com/documentation/file.SASS_REFERENCE.html) 

