<?php
	use League\Route\Strategy\RequestResponseStrategy;
	class Router
	{
		public $db;
		public $router;
		public $tables;
		function __construct() {
			$this->db=db_connect();
			$this->router = new League\Route\RouteCollection;
			$this->tables=get_tables('Entity');
			$this->insert_admin_routes();
			try {
			 	$mapper = $this->db->mapper ( 'Entity\Setting' );
			 	$default_language = $mapper->where ( ['Title' => 'Default Language'] )->first ();
			 	if($default_language){set_default_locale($default_language->value);}
			 	$this->insert_theme_routes();
			 } catch (Exception $e) {}
			 $this->dispatche();
		}
		public function dispatche()
		{
			$uri = get_parsed_uri();
			$dispatcher = $this->router->getDispatcher();
			$response = $dispatcher->dispatch('POST', $uri);
			$response->send();	
		}
		public function insert_admin_routes()
		{
			/*Admin Routes*/
			$this->router->addRoute('POST', ADMIN_PATH.'/login', 'Admin\Controller::login',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/authenticate', 'Admin\Controller::authenticate',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/logout', 'Admin\Controller::logout',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/dashboard', 'Admin\Controller::dashboard',new RequestResponseStrategy);	
			$this->router->addRoute('POST', ADMIN_PATH.'/mailer', 'Admin\Controller::mailer',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/captcha', 'Admin\Controller::captcha',new RequestResponseStrategy);
			/*CRUD Operations*/
			$this->router->addRoute('POST', ADMIN_PATH.'/migrations', 'Admin\Controller::migrations',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/default-insertions', 'Admin\Controller::default_insertions',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/generate-active-theme', 'Admin\Controller::generate_active_theme',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/install', 'Admin\Controller::install',new RequestResponseStrategy);
			/*Media files Operations */
			$this->insert_media_routes();
			/*Adding entity routes using the entity table name*/
			foreach ($this->tables as $table) {
				$this->insert_entity_routes($table);
			}
		}
		public function insert_media_routes(){
			$this->router->addRoute('POST', ADMIN_PATH.'/media', 'Admin\Controller::media',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/media_box', 'Admin\Controller::media_box',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/media/upload', 'Admin\Controller::media_upload',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/media/delete/{name}/{ext}', 'Admin\Controller::media_delete',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/media_box/delete/{name}/{ext}', 'Admin\Controller::media_box_delete',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/galleries/media/{id}', 'Admin\Controller::galleries_media',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/galleries/media/delete/{name}/{ext}/{title}/{id}/{mode}', 'Admin\Controller::galleries_media_delete',new RequestResponseStrategy);
		}
		public function insert_entity_routes($name)
		{
			$this->router->addRoute('POST', ADMIN_PATH.'/'.$name, 'Admin\Controller::all',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/'.$name.'/add', 'Admin\Controller::add',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/'.$name.'/edit/{id}', 'Admin\Controller::edit',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/'.$name.'/insert', 'Admin\Controller::insert',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/'.$name.'/delete/{id}', 'Admin\Controller::delete',new RequestResponseStrategy);
			$this->router->addRoute('POST', ADMIN_PATH.'/'.$name.'/update/{id}', 'Admin\Controller::update',new RequestResponseStrategy);
		}		
		public function insert_theme_routes()
		{
			/*Theme Routes*/
			$mapper = $this->db->mapper('Entity\Page');
			$pages=$mapper->all();
			for($i= 0;$i < count($pages);$i++){
				$this->router->addRoute('POST', $pages[$i]->uri, 'Theme\Controller::action',new RequestResponseStrategy);
			}
			$this->router->addRoute('POST', '/{uri}', 'Theme\Controller::not_found',new RequestResponseStrategy);
		}

	}	
?>