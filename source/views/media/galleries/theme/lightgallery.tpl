<div class="light-gallery">
	<?php foreach($images as $key => $image): ?>
		<a class="gallery-item" rel=group href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$image?>">
			<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$thumbnails[$key]?>" alt="<?php echo $gallery->title;?>"><span></span>
		</a>			
	<?php endforeach ?>
</div>	
