<div class="masonry-gallery">
	<?php $j=1;foreach($images as $key => $image): ?>
		<div class="gallery-item <?php if($j==1){ echo 'style1';}else if($j==2){echo 'style2';}else if($j==3){echo 'style3';}else{echo 'style4';}; ?>">
			<?php if ($gallery->popup == "None"): ?>
				<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$thumbnails[$key]?>" alt="<?php echo $gallery->title;?>">
			<?php else: ?>
				<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$image?>">
					<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$thumbnails[$key]?>" alt="<?php echo $gallery->title;?>">
				</a>			
			<?php endif; ?>	
		</div>
		<?php if($j==7){$j=0;}$j=$j+1; ?>		
	<?php endforeach ?>
</div>	