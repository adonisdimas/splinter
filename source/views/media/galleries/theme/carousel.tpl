<div class="carousel-gallery">
	<?php foreach($images as $key => $image): ?>
		<div class="gallery-item">
			<?php if ($gallery->popup == "None"): ?>
				<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$thumbnails[$key]?>" alt="<?php echo $gallery->title;?>"><span></span>
			<?php else: ?>
				<a rel=group class="<?php echo $gallery->popup;?>" href="<?php echo $galleries_path.'/'.$gallery->title.'/'.$image?>">
					<img src="<?php echo ROOT.'/'.GALLERIES_PATH.'/'.$gallery->title.'/'.$thumbnails[$key]?>" alt="<?php echo $gallery->title;?>"><span></span>
				</a>			
			<?php endif; ?>	
		</div>
	<?php endforeach ?>
</div>	
