<form class="contact-form" method="post" action='<?=ROOT?><?=ADMIN_PATH?>/mailer'>
		    <div class="form-group row">
		    	<div class="col-md-6">
		    		<label for="Name"><?php echo $translations['Contact Form Name']; ?>*</label>
		    		<input type="text" name="Name" required="">
		    	</div>
			    <div class="col-md-6">
			    	<label for="Surname"><?php echo $translations['Contact Form Surname']; ?>*</label>
			    	<input type="text" name="Surname" required="">
			    </div>
		    </div>
		    <div class="form-group row">
		    	<div class="col-md-6">
		    		<label for="Email"><?php echo $translations['Contact Form Email']; ?>*</label>
		    		<input type="email" name="Email" required="">
		    	</div>
			    <div class="col-md-6">
			    	<label for="Telephone"><?php echo $translations['Contact Form Telephone']; ?>*</label>
			    	<input type="tel" name="Telephone" required="">
			    </div>
		    </div>
		    <div class="form-group row">
		    	<label for="message"><?php echo $translations['Contact Form Message']; ?></label>
		    	<textarea name="message" rows="10" cols="30" required=""></textarea>
		    </div>
		    <div class="form-group row">
				<div class="col-md-4">
					<label for="upload"><?php echo  $translations['Contact Form Upload']; ?></label>
           			<input type="file" name="upload"></input>
       			</div>
		    	<div class="col-md-4">
		    		<label for="code"><?php echo $translations['Contact Form Code']; ?>*</label>
		    		<input type="text" name="code" id="sec_code" class="textfield_2">
		    		<img class="sec_code" src="<?=ROOT.ADMIN_PATH?>/captcha?randstring=<?php echo rand(999,9999); ?>" alt="verification image, type it in the box" name="ver_img" width="40" height="18" vspace="0" align="top" id="ver_img">
		    	</div>
				<div class="col-md-4">
					<label for="upload"></label>
				    <input type="submit" value="<?php echo $translations['Contact Form Submit']; ?>">
				</div>
		    </div>
		    <?php if(isset( $_GET['send'])&& $_GET['send'] == -1){?>
		    <div style="background-color:#D70000; color:#FFFFFF; height:30px; padding:0px; padding-left:5px;width:185px;margin-left:0px;font-size:10px;font-size: 10px;position:absolute;bottom: 0px;
		    right: 30px;">
		    <div align="center"><?php echo $translations['Contact Form Failure Message']; ?></div>
		    </div>
		    <?php ;}?>
		    <?php if(isset( $_GET['send'])&&$_GET['send'] == 1){?>
		    <div style="background-color:#83831f; color:#FFFFFF; padding:0px; height:30px; padding-left:5px;width:185px;margin-left:0px;position: absolute;font-size: 10px;position:absolute;bottom: 0px;
		    right: 30px;">
		    <div><?php echo $translations['Contact Form Success Message']; ?></div>
		    </div>
		    <?php ;}?>
		    <br>
     		<label><span>*<?php echo $translations['Contact Form Required']; ?></span></label>
     		<input type="hidden" name="uri" value="/carrers" >
</form>	