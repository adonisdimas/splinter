<?php $this->layout('base', ['page_title'=>$page_title,'page_translation' =>$page_translation,'locale' => $this->e($locale),'regions' => $regions,'theme_assets' => $theme_assets, 'theme_assets_path' => $theme_assets_path,'sliders_assets' => $sliders_assets, 'sliders_path' => $sliders_path,'galleries_assets' => $galleries_assets, 'galleries_path' => $galleries_path,'settings' => $settings]) ?>
<?php $this->start('main-content') ?>
	<main>	
	<div class="row slider-wrapper">
		<?php if (isset($sliders['pages'])): ?>	
			<?php foreach($sliders['pages'] as $key => $slider): ?>
	  				<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>						
	 		<?php endforeach ?>
		<?php endif; ?>		
	</div>
		<div class="row">
			<div class="content-padding content-width-inner content-padding">
				<div class="row top-bottom-padding">
					<div class="col-md-12 center-align"><h1><?=$page_translation->title;?></h1></div>
					<?php $this->insert('components/page-content', ['page_content' => $page_contents[0]]); ?>			
				</div>
			</div>
		</div>	
		<div class="row">
			<?php if (isset($page_contents)): ?>	
				<?php foreach($page_contents as $key => $page_content): ?>
					<?php if($key>0): ?>
						<div class="col-md-12">
							<?php $this->insert('components/page-content', ['page_content' => $page_content]); ?>					
						</div>
					<?php endif; ?>								
		 		<?php endforeach ?>
			<?php endif; ?>								
		</div>
		<?php if (isset($regions['contact-form'])): ?>
			<div class="row">
				<div class="content-padding content-width-inner2 content-padding">
				<?php $this->insert('regions/contact-form', ['region' =>$regions['contact-form'],'translations' =>$translations]); ?>
			    </div>
			</div>	
		<?php endif; ?>	
		<?php if (isset($regions['contact-cv-form'])): ?>
			<div class="row">
				<div class="content-padding content-width-inner2 content-padding">
				<?php $this->insert('regions/contact-cv-form', ['region' =>$regions['contact-cv-form'],'translations' =>$translations]); ?>
			    </div>
			</div>	
		<?php endif; ?>	
		<?php if (isset($regions['section-approach'])): ?>
			<div class="row approach-section">
				<?php $this->insert('regions/section-approach', ['region' =>$regions['section-approach']]); ?>
			</div>	
		<?php endif; ?>	
		<?php if (isset($regions['section-services'])): ?>
			<div class="row services-section">
				<?php $this->insert('regions/section-services', ['region' =>$regions['section-services']]); ?>
			</div>	
		<?php endif; ?>	
		<?php if (isset($regions['section-contact'])): ?>
			<div class="row contact-section">
				<?php $this->insert('regions/section-contact', ['region' =>$regions['section-contact']]); ?>
			</div>	
		<?php endif; ?>	
		 <?php if (isset($regions['content'])): ?>
			<div class="row scroll-section">
				<?php $this->insert('regions/content', ['region' =>$regions['content']]); ?>
			</div>	
		<?php endif; ?>		
</main>
<?php $this->stop() ?>