<ul class="<?=$classes?>">
	<?php if ($title=='Main Menu'): ?>
		<li><a href="<?php echo $menu[0]->link;?>" <?php if($page_title =='Home'){ echo 'class="active"';}?>><?php echo $menu[0]->title;?></a></li>
		<li><a href="<?php echo $menu[1]->link;?>" <?php if($page_title =='Approach'){ echo 'class="active"';}?>><?php echo $menu[1]->title;?></a></li>
		<li>
			<a href="#" <?php if($page_title =='Services Business Planning' or $page_title =='Services Business Development'){ echo 'class="active"';}?>><?php echo $menu[2]->title;?></a>
			<ul class="submenu">
				<li><i class="fa fa-caret-right" aria-hidden="true"></i>
				<a href="<?php echo $menu[3]->link;?>" <?php if($page_title =='Services Business Planning'){ echo 'class="active"';}?>><?php echo $menu[3]->title;?></a></li>
				<li><i class="fa fa-caret-right" aria-hidden="true"></i>
				<a href="<?php echo $menu[4]->link;?>" <?php if($page_title =='Services Business Development'){ echo 'class="active"';}?>><?php echo $menu[4]->title;?></a></li>
			</ul>
		</li>
		<li><a href="<?php echo $menu[5]->link;?>" <?php if($page_title =='Careers'){ echo 'class="active"';}?>><?php echo $menu[5]->title;?></a></li>
		<li class="last"><a href="<?php echo $menu[6]->link;?>" <?php if($page_title =='Contact'){ echo 'class="active"';}?>><?php echo $menu[6]->title;?> </a></li>	
	<?php else: ?>
		<?php foreach($menu as $key => $menu_item): ?>
	  		<li <?php if(count($menu)==$key+1){echo 'class="last"';}?>>
	  			<a href="<?=$menu_item->link?>" <?=$this->uri($menu_item->link, 'class="active"')?>><?=$menu_item->title?></a>
	  		</li>
	 	<?php endforeach ?>	
	<?php endif; ?>
</ul>