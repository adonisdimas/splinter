<div class="row caption">
	<?php echo $region['translations']['Caption']; ?>
</div>		
<?php if(isset($sliders)): ?>	
	<?php foreach($sliders as $key => $slider): ?>
 		<?php	$this->insert($sliders_path.'/theme/'.$slider[0]->type, ['sliders_path' =>$sliders_path,'slider' => $slider]); ?>						
	<?php endforeach ?>
	<a href="#" class="scroll-down"><img src="/<?=$theme_assets_path?>/img/icons/arrow-down5.png" /></a>			
<?php endif; ?>
<ul class="language bar">
	<li><a class="active" href="/en/">EN</a></li>
	<li class="last"><a href="/el/">GR</a></li>
</ul>
<?php foreach($region['menus'] as $menu): ?>
	<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>						
<?php endforeach ?>