<?php if (isset($region['menus'])): ?>
	<div class="row">
		<?php foreach($region['menus'] as $menu): ?>
			<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>						
		<?php endforeach ?>
	</div>
<?php endif; ?>
<? if (isset($region['blocks'])): ?>
	<div class="row">
		<?php foreach($region['blocks'] as $block): ?>
		<div class="<?=$block['contents'][0]?>">
			<?php if($block['contents'][1][0]->hide_title!=1): ?>
				<h2><?=$block['contents'][1][0]->title?></h2>
			<?php endif ?>
			<?=$block['contents'][1][0]->content?>
		</div>		
		<?php endforeach ?>
	</div>
<? endif; ?>