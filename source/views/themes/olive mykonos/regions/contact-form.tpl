<form class="contact-form" method="post" action='<?=ROOT.ADMIN_PATH?>/mailer'>
    <div class="form-group row">
    	<div class="col-md-6">
    		<input type="text" name="firstname" placeholder="<?php echo $region['translations']['Contact Form Name']; ?>" required="">
    	</div>
	    <div class="col-md-6">
	    	<input type="email" name="email" placeholder="<?php echo $region['translations']['Contact Form Email']; ?>" required="">
	    </div>
    </div>
    <div class="form-group row">
    	<textarea name="message" rows="10" cols="30" placeholder="<?php echo $region['translations']['Contact Form Message']; ?>" required=""></textarea>
    </div>
    <div class="form-group row">
    	<div class="col-md-9">
    		<input type="text" name="code" id="sec_code" placeholder="<?php echo $region['translations']['Contact Form Code']; ?>" class="textfield_2">
    		<img class="sec_code" src="<?=ROOT.ADMIN_PATH?>/captcha?randstring=<?php echo rand(999,9999); ?>" alt="verification image, type it in the box" name="ver_img" width="40" height="18" vspace="0" align="top" id="ver_img">
    	</div>
    	<div class="col-md-3">
    		<input type="submit" value="<?php echo $region['translations']['Contact Form Submit']; ?>">
    	</div>
    </div>
    <?php if($_GET['send'] == -1){?>
    <div style="background-color:#D70000; color:#FFFFFF; height:30px; padding:0px; padding-left:5px;width:185px;margin-left:0px;font-size:10px;font-size: 10px;position:absolute;bottom: 0px;
    right: 30px;">
    <div align="center"><?php echo $region['translations']['Contact Form Failure Message']; ?></div>
    </div>
    <?php ;}?>
    <?php if($_GET['send'] == 1){?>
    <div style="background-color:#83831f; color:#FFFFFF; padding:0px; height:30px; padding-left:5px;width:185px;margin-left:0px;position: absolute;font-size: 10px;position:absolute;bottom: 0px;
    right: 30px;">
    <div><?php echo $region['translations']['Contact Form Success Message']; ?></div>
    </div>
    <?php ;}?>
    <br>
</form>