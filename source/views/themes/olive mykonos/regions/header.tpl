<header>	
	<div class="row">
		<div class="content-padding content-width">
			<div class="row">
				<div class="col-md-2 logo">
					<a href="/"></a>
				</div>
				<div class="col-md-8 navigation content-padding">
					<div class="nav-bar1">
						<div id="dropdown"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></div>
						<nav class="menu">
						<?php if (isset($region['menus'])): ?>
							<?php foreach($region['menus'] as $menu): ?>
								<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>						
							<?php endforeach ?>
						<?php endif; ?>
						</nav>
					</div>
				</div>
				<div class="col-md-2 book-online-bg">
					<a target="_blank" href="<?php echo $settings['Book Online Link']; ?>"><?php echo $region['translations']['Book Online label'];?></a>
				</div>
			</div>
			<?php if (isset($region['blocks'])): ?>
				<div class="row">
					<?php foreach($region['blocks'] as $block): ?>
						<?php	$this->insert('components/block', ['classes'=>$block['contents'][0],'block' => $block['contents'][1]]); ?>
					<?php endforeach ?>
				</div>
			<?php endif; ?>
		</div>
	</div>			
</header>