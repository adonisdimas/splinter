<footer>
	<div class="row top bg-pattern2">
		<div class="content-width content-padding">
			<?php if (isset($region['blocks'])): ?>
			  <div class="row">
				<?php foreach($region['blocks'] as $block): ?>
						<?php $this->insert('components/block', ['classes' => $block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			  </div>
			<?php endif; ?>
			<?php if (isset($region['menus'])): ?>
			 	<div class="row">
					<?php foreach($region['menus'] as $menu): ?>
							<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>	
					<?php endforeach ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="row bottom">
		<div class="content-width content-padding">
			<div class="row">
				<div class="col-md-6 col-sm-6 copy">
					<p> &copy; <?php echo date("Y") ?> | <?php echo $region['translations']['Copyright']; ?></p>
				</div>
				<div class="col-md-6 col-sm-6 created">
					<p><?php echo $region['translations']['Created by']; ?> <a target="new" href="www.dproject.gr">Dproject</a></p>
				</div>
			</div>
		</div>
	</div>	
</footer>