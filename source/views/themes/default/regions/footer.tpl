<footer>
	<div class="row top background1">
		<div class="content-width-inner content-padding">
			<?php if (isset($region['blocks'])): ?>
			  <div class="row">
				<?php foreach($region['blocks'] as $block): ?>
						<?php $this->insert('components/block', ['classes' => $block['contents'][0],'block' => $block['contents'][1]]); ?>
				<?php endforeach ?>
			  </div>
			<?php endif; ?>
			<?php if (isset($region['menus'])): ?>
			 	<div class="row">
					<?php foreach($region['menus'] as $menu): ?>
							<?php	$this->insert('components/menu', ['classes'=>$menu['contents'][0],'menu' => $menu['contents'][1]]); ?>	
					<?php endforeach ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="row bottom">
		<div class="content-width-inner content-padding">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6 copy">
					<p>� 2017 | <?php echo $region['translations']['Copyright']; ?></p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 created">
					<p><?php echo $region['translations']['Created']; ?> <a target="new" href="www.dproject.gr">Dproject</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>