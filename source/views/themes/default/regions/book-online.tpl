<div class="book-online">
	<form class="book-online-form content-width form-inline" id="booking_form" name="booking_form" method="post" action="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>" target="new">
		<div class="form-group check">
			<div class="row">
			 <div class="col-md-3"><img src="<?php echo $theme_assets_path.'/img/check.png';?>" /></div>
			 <div class="col-md-8"><span><?php echo $region['translations']['Book Online label'];?></span></div>
			</div>
		</div>
		<div class="form-group">
	      <input type="text" placeholder="<?php echo $region['translations']['Book Online form label 1'];?>" class="calender" id="datepicker1">
	    </div>
		<div class="form-group">
	      <input type="text" placeholder="<?php echo $region['translations']['Book Online form label 2'];?>" class="calender" id="datepicker2">
	    </div>
	   	<div class="form-group">
			<input type="number" placeholder="<?php echo $region['translations']['Book Online form label 3'];?>" min="1" max="3" step="1"name="rooms" class="form-select" id="rooms">
		</div>
		<div class="form-group">
			<input type="number" placeholder="<?php echo $region['translations']['Book Online form label 4'];?>" min="1" max="2" step="1" name="adults" class="form-select" id="adults">
		</div>
		<div class="form-group">
			<input type="number" placeholder="<?php echo $region['translations']['Book Online form label 5'];?>" min="0" max="1" step="1" name="children" class="form-select last" id="children">
		</div>
		<div class="button"><?php echo $region['translations']['Book Online button label'];?></span></div>
	</form>
	<div class="online-button"><a href="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>" target="new" title="overlay" style=""></a></div>
</div>
<a id="book-online-resp" target="new" href="<?php if (isset($settings['Book Online Link'])){ echo $settings['Book Online Link'];} ?>"><?php echo $region['translations']['Book Online button label'];?></span></a>	