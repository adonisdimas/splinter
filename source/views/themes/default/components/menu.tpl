<ul class="<?=$classes?>">
	<?php foreach($menu as $menu_item): ?>
  		<li <?=$this->uri($menu_item->link, 'class="active"')?>><a href="<?=$menu_item->link?>"><?=$menu_item->title?></a></li>
 	<?php endforeach ?>
</ul>