function ValidateDates11a(lang) {
				arrival_date="" + document.getElementById("datepicker1").value;
				departure_date="" + document.getElementById("datepicker2").value;
				myrooms = document.getElementById('rooms').value;
				myadults = document.getElementById('adults').value;
				mykids = document.getElementById('children').value;

				if(myrooms.length==0 ||  isNaN(myrooms)){
				  myrooms=1
				}
				if(myadults.length==0 ||  isNaN(myadults)){
				  myadults=2
				}
				if(mykids.length==0 || isNaN(mykids)){
				  mykids=0
				}

				switch(lang) {
				case "en":
					document.getElementById('booking_form').action = unescape("https://athensdiamondplus.reserve-online.net/?Page=19&lan_id=&checkin="+arrival_date+"&checkout="+departure_date+"&rooms="+myrooms+"&adults="+myadults+"&kids="+mykids+"&kid1=-1&kid2=1&kid3=-1&extra=0&cot=0");
				break;
				case "el":
					document.getElementById('booking_form').action = unescape("https://athensdiamondplus.reserve-online.net/?Page=19&lan_id=el-GR&checkin="+arrival_date+"&checkout="+departure_date+"&rooms="+myrooms+"&adults="+myadults+"&kids="+mykids+"&kid1=-1&kid2=1&kid3=-1&extra=0&cot=0");
				break;

				}


				var d1 = new Date(arrival_date.substr(3,2) + "/" + arrival_date.substr(0,2) + "/" + arrival_date.substr(6,4));
				var d2 = new Date(departure_date.substr(3,2) + "/" + departure_date.substr(0,2) + "/" + departure_date.substr(6,4));
				var todayD=new Date();

				var d1_month = d1.getMonth() + 1; if(d1_month<10) d1_month="0" + d1_month;
				var d1_day = d1.getDate(); if(d1_day<10) d1_day="0" + d1_day;
				var d1_year = d1.getFullYear();	var d1_full=d1_year + '' + d1_month + '' + d1_day;

				var d2_month = d2.getMonth() + 1; if(d2_month<10) d2_month="0" + d2_month;
				var d2_day = d2.getDate(); if(d2_day<10) d2_day="0" + d2_day;
				var d2_year = d2.getFullYear();	var d2_full=d2_year + '' + d2_month + '' + d2_day;

				var todayD_month = todayD.getMonth() + 1; if(todayD_month<10) todayD_month="0" + todayD_month;
				var todayD_day = todayD.getDate(); if(todayD_day<10) todayD_day="0" + todayD_day;
				var todayD_year = todayD.getFullYear();	var todayD_full=todayD_year + '' + todayD_month + '' + todayD_day;

				if(d2_full<=d1_full) {
					if (lang=="el") {
						alert("Ελέξτε ότι καταχωρήσατε τις ημερομηνίες σωστά");
					} else {
						alert("Please confirm that you enter the dates correctly");
					}
					return false;
				}
				if(d2_full<todayD_full) {
					if (lang=="el") {
						alert("Ελέξτε ότι καταχωρήσατε τις ημερομηνίες σωστά");
					} else {
						alert("Please confirm that you enter the dates correctly");
					}
					return false;
				}
				if(d1_full<todayD_full) {
					if (lang=="el") {
						alert("Ελέξτε ότι καταχωρήσατε τις ημερομηνίες σωστά");
					} else {
						alert("Please confirm that you enter the dates correctly");
					}
					return false;
				}

				return true;
}