/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo(message) {
	if (settings.debug) {
		console.log(message);
	}
};
/* IE detection flag */
jQuery.browser = {};
(function() {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();
/* Global help-vars */
var resizeTimeout;
(function($) {
	var app = {
		'init' : function(e) {
			echo('App init...');
			app.manageStickyHeader();
			app.manageLanguageSelector();
			app.manageOffer();
			app.manageAnimations();
			app.manageSlider();
			app.manageGalleries();
			app.manageDropdown();
			app.manageBookOnline();
			app.manageScroll();
			app.manageGooglemap();
			$(window).on('resize', app.onResize);
		},
		'manageLanguageSelector' : function() {
		    $('.selectpicker').selectpicker();
		},
		'manageStickyHeader' : function() {
			var header = $('header');
		    $(window).scroll(function () {
	    		if ($(this).scrollTop() > 100) {
		            header.addClass("sticky");
	    		}else{
	    			header.removeClass("sticky");
	    		}
		    });
		},
		'manageOffer' : function() {
			if ( $("#offer-modal" ).length ) {
				$('#offer-modal').modal('show');
			}
		},
		'manageAnimations' : function() {
			 if($(window).width()>992){
				setTimeout(function(){
					$('.slider-wrapper .caption').addClass('animated zoomIn');
					$('.slider-wrapper .caption').show();
				}, 1200);
			 }
		},
		'manageDropdown' : function() {
			$("#dropdown").on("click", function(){
				$("header .menu").slideToggle();
				$(this).toggleClass("active");
			});
		},
		'manageSlider' : function() {
			  if($('.page-slider').length){
				 $('.page-slider').fotorama({
					  maxwidth: '100%',
					  width: '100%',
					  height:'auto',
					  maxheight: '100%',
					  fit:'cover',
					  allowfullscreen: false,
					  transition : 'fade',
					  nav:false,
					  arrows:true
				  });
			  }
		},
		'manageGalleries' : function() {
            if ($('#gallery').length){
	            	$('#gallery').masonry({
                      itemSelector: '.photo-item',
                      columnWidth:1,
                      gutter:5,
                      fitWidth: true
                    });
	                $("#gallery a.gallery-thumb[rel=group]").fancybox({
	            		'transitionIn'		: 'elastic',
	            		'transitionOut'		: 'fade',
	            		'titlePosition' 	: 'over',
	            		'padding' : 0,
	            		'autoScale'		: true
	            	});
            }
            if ($('.page-gallery').length){		
			  $(".page-gallery").owlCarousel({
				  	items:4,
				    margin:2,
				    loop:true,
				    navigation : false,
				    pagination : false,
				    responsive:{
				        0:{
				            items:1,
				            nav:false
				        },
				        400:{
				            items:2,
				            nav:false
				        },
				        600:{
				            items:3,
				            nav:false
				        },
				        999:{
				            items:4,
				            nav:false
				        },  
				    }
			 
			  });
			$('.page-gallery a.gallery-thumb').fancybox({
        		'transitionIn'		: 'elastic',
        		'transitionOut'		: 'fade',
        		'titlePosition' 	: 'over',
        		'padding' : 0,
        		'overlayColor' : '#000000',
        		'overlayOpacity' : '0.6',
        		'autoScale'		: true
			});
            }
            if ($('.teaser-gallery').length){		
				$(".teaser-gallery .teaser-item").on("click", function(e){
					e.preventDefault();
					$(this).find('a')[0].click();
				});	
				  $(".teaser-gallery").owlCarousel({
					  	items:4,
					    margin:2,
					    loop:true,
					    navigation : false,
					    pagination : false,
					    responsive:{
					        0:{
					            items:1,
					            nav:false
					        },
					        400:{
					            items:2,
					            nav:false
					        },
					        600:{
					            items:3,
					            nav:false
					        },
					        999:{
					            items:4,
					            nav:false
					        },  
					    }
				 
				  });	
            
            }
            
		},
		'manageBookOnline' : function() {
			$("a.book-mobile").on("click", function(e){
				 e.preventDefault();
				$(".book-online-form").toggle();
				//$("header .menu").css( "display", "block");
				$(this).toggleClass("active");
			});			
            $('#datepicker1').datepicker({
                format: 'mm dd yyyy',
                minDate: 0
            });
            $('#datepicker2').datepicker({
                format: 'mm dd yyyy',
                minDate: 0
            });
		},
		'manageScroll' : function() {
			//Check to see if the window is top if not then display button
			$(window).scroll(function(){
				if($(window).width()>1200){
					if ($(this).scrollTop() > 100) {
						$('.scrollToTop').fadeIn();
					} else {
						$('.scrollToTop').fadeOut();
					}
			     }
			});
			//Click event to scroll to top
			$('.scrollToTop').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : 0},800);
				return false;
			});
			//Click event to scroll to top
			$('.scroll-down').click(function(e){
				e.preventDefault();
				$('html, body').animate({scrollTop : $(".scroll-section").offset().top-180},800);
				return false;
			});
		},
		'manageGooglemap' : function() {	
			if ( $( "#map-canvas" ).length ) {
                /* Custom google map without the use of API KEY*/
                window._alert = window.alert;
                window.alert = function() {return;};
                window.setupMap = function() {
                	 var infowindow = new google.maps.InfoWindow({
                		    content: "<strong>Casa del Mare</strong> <br> , Greece"
                	 });
					var mapOptions = {
						zoom: 16,
						center: new google.maps.LatLng(37.697854, 23.951079),
						mapTypeControl: true,
						zoomControl: true,
						scrollwheel: false,
	    				disableDefaultUI: false						
					};
					var map = new google.maps.Map(document.getElementById('map-canvas'),
					  mapOptions);
					window.map = map;
					var customIcon = 'img/icons/pin.png';
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(37.697854, 23.951079),
						icon: customIcon,
						map: map
					});		
	          		marker.addListener('click', function() {
	          		   infowindow.open(map, marker);
	          		});
					var styleOptions = {
							name: "Map Style"
					};
			        // Create a new StyledMapType object, passing it an array of styles,
			        // and the name to be displayed on the map type control.
			        var styledMapType = new google.maps.StyledMapType(
			          [
			            {
			              stylers: [
			                { hue: '#00ffe6' },
			                { saturation: -20 }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'geometry',
			              stylers: [
			                { lightness: 100 },
			                { visibility: 'simplified' }
			              ]
			            },{
			              featureType: 'road',
			              elementType: 'labels',
			              stylers: [
			                { visibility: 'off' }
			              ]
			            }
			          ],
			          {name: 'Styled Map'});
					var mapType = new google.maps.StyledMapType(styledMapType, styleOptions);
					map.mapTypes.set("Map Style", mapType);
					map.setMapTypeId("Map Style");							
					};

                window.onload = function() {setupMap()};	
				$("a#map-popup").fancybox({
					'hideOnContentClick': true,
					'overlayColor'		: '#6b0202',
					'overlayOpacity'	: 0.8,
				    beforeShow: function () {
				        google.maps.event.trigger(window.map, "resize");
				        window.map.setZoom(14);
				    }
				});
			}
		},
		'onResize' : function() {
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
				if ( !$('#dropdown').hasClass('active') ) {
					 if($(window).width()>1140){
						$("header .menu").css( "display", "block");
				    }else{
						$("header .menu").css( "display", "none");
				     }
				}
			}, 10);
		}
	};
	$(document).on('ready', app.init);
})(jQuery);
