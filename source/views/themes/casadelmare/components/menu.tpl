<ul class="<?=$classes?>">
	<?php foreach($menu as $menu_item): ?>
  		<li><a href="<?=$menu_item->link?>" <?=$this->uri($menu_item->link, 'class="active"')?>><?=$menu_item->title?></a></li>
 	<?php endforeach ?>
</ul>