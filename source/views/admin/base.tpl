<?php $this->layout('layout', ['title' => $this->e($title),'locale' => $this->e($locale)]) ?>
<?php $this->start('head') ?>
<?php $this->stop() ?>
<?php $this->start('header') ?>
	<?php $this->insert('regions/header', ['username' => $this->e($username)]); ?>
<?php $this->stop() ?>
<?php $this->start('main-content') ?>
<div class="main-content">
	<?php $this->insert('regions/side-menu', ['active' => $active, 'post_types' => $post_types]); ?>
<div class="container-fluid">
  <div class="side-body">
	<?php $this->insert('regions/messages'); ?>
	<?=$this->section('page')?>
  </div>
</div>
</div>
<?php $this->stop() ?>
<?php $this->start('footer') ?>
<p></p>
</br>
<?php $this->stop() ?>
<?php $this->start('scripts') ?>
<?php $this->stop() ?>