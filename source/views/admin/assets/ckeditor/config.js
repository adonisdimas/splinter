/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	config.uiColor = '#f1f1f1';
	config.allowedContent = true;
	config.entities = false;
	config.extraAllowedContent = 'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*}';
	config.height = '350px';
	config.filebrowserBrowseUrl = media_path;
	config.filebrowserWindowWidth =  '1200';
    config.filebrowserWindowHeight =  '600';
	config.format_tags = 'p;h1;h2;h3;pre';
	config.extraPlugins = 'ckeditor-gwf-plugin';
};
CKEDITOR.dtd.$removeEmpty.i = 0;
