<header>
	<div class="row"> 
		<nav class="navbar navbar-default navbar-static-top navbar-inverse navbar-fixed" role="navigation">
		    <div class="navbar-header">
		        <div class="brand-wrapper">
		            <div class="brand-name-wrapper">
		                <a class="navbar-brand" href="<?=ROOT.ADMIN_PATH?>/dashboard"><i class="fa fa-cube" aria-hidden="true"></i>Splinter CMS</a>
		            </div>
		        </div>
		    </div>
		    <ul class="nav navbar-right top-nav">
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle"  data-toggle="dropdown"><i class="fa fa-user"></i> <?=$username?> <b class="caret"></b></a>
		            <ul class="dropdown-menu">
		                <li>
		                    <a href="<?=ROOT.ADMIN_PATH?>/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
		                </li>
		            </ul>
		        </li>
		    </ul>
		</nav>
	</div>
</header>