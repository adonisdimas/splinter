<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username),'active' =>$active, 'post_types' =>$post_types]) ?>
<?php $this->start('page') ?>
<div class="container-fluid">
		<h1><?=$this->e($title)?></h1>
		<div class="row">
			<div class="col-md-6">
				  <h3>Users:</h3>        
				  <table class="table">
				    <thead>
				      <tr>
				        <th>Username</th>
				        <th>Email</th>
				        <th>Status</th>
				        <th>Operations</th>
				      </tr>
				    </thead>
				    <tbody>
						<?php foreach($users as $key => $entity): ?>
						  <tr>
					        <td><?=$entity->username?></td>
					        <td><?=$entity->email?></td>
					        <td><?=$entity->status?></td>
					        <td><a href="users/edit/<?=$entity->id?>">Edit</a> <?php if(count($users)>1): ?>| <a onclick = "if (! confirm('Continue?')) { return false; }" href="users/delete/<?=$entity->id?>">Delete</a><?php endif; ?></td>
					      </tr>
					    <?php endforeach ?>
				    </tbody>
				  </table>
			</div>
			<div class="col-md-6">
				  <h3>Languages:</h3>      
				  <table class="table">
				    <thead>
				      <tr>
				        <th>Locale</th>
				        <th>Title</th>
				      </tr>
				    </thead>
				    <tbody>
						<?php foreach($languages as $key => $entity): ?>
						  <tr>
					        <td><?=$entity->locale?></td>
					        <td><?=$entity->title?></td>
					        <td><a href="languages/edit/<?=$entity->id?>">Edit</a> <?php if(count($languages)>1): ?>| <a onclick = "if (! confirm('Continue?')) { return false; }" href="languages/delete/<?=$entity->id?>">Delete</a><?php endif; ?></td>
					      </tr>
					    <?php endforeach ?>
				    </tbody>
				  </table>
			</div>	
			<div class="col-md-6">		
				<h3>Menus:</h3>
				  <table class="table">
				    <thead>
				      <tr>
				        <th>Title</th>
				        <th>Classes</th>
				        <th>Order</th>
				        <th>Region Assigned</th>
				        <th>Operations</th>
				      </tr>
				    </thead>
				    <tbody>
						<?php foreach($menus as $key => $entity): ?>
						  <tr>
					        <td><?=$entity->title?></td>
					        <td><?=$entity->classes?></td>
					        <td><?=$entity->order?></td>
					        <td><?=$entity->region_title?></td>
					        <td><a href="<?=ADMIN_PATH?>/menu_items/edit/<?=$entity->id?>">Add/Edit Menu Items</a> | <a href="menus/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="menus/delete/<?=$entity->id?>">Delete</a></td>
					      </tr>
					    <?php endforeach ?>
				    </tbody>
				  </table>	
			</div>
			<div class="col-md-6">
				<div class="row">
					<h3>Blocks:</h3>
					<div class="table-responsive">          
					  <table class="table">
					    <thead>
					      <tr>
					        <th>Title</th>
					        <th>Classes</th>
					        <th>Order</th>
					        <th>Region Assigned</th>
					        <th>Operations</th>
					      </tr>
					    </thead>
					    <tbody>
							<?php foreach($blocks as $key => $entity): ?>
							  <tr>
						        <td><?=$entity->title?></td>
						        <td><?=$entity->classes?></td>
						        <td><?=$entity->order?></td>
						        <td><?=$entity->region_title?></td>
						        <td><a href="blocks/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="blocks/delete/<?=$entity->id?>">Delete</a></td>
						      </tr>
						    <?php endforeach ?>
					    </tbody>
					  </table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12"><h3>Pages:</h3>
				<div class="table-responsive">          
				  <table class="table">
				    <thead>
				      <tr>
				        <th>Title</th>
				        <th>Uri</th>
				        <th>Template Assigned</th>
				        <th>Operations</th>
				      </tr>
				    </thead>
				    <tbody>
						<?php foreach($pages as $key => $entity): ?>
						  <tr>
					        <td><?=$entity->title?></td>
					        <td><?=$entity->uri?></td>
					        <td><?=$entity->template_title?></td>
					        <td><a href="<?=ADMIN_PATH?>/pages_translation/edit/<?=$entity->id?>">Add/Edit Content</a> | <a href="pages/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="pages/delete/<?=$entity->id?>">Delete</a></td>
					      </tr>
					    <?php endforeach ?>
				    </tbody>
				  </table>
				</div>
			</div>	
			<div class="col-md-12"><h3>Post Types:</h3>
				<div class="table-responsive">          
				  <table class="table">
				    <thead>
				      <tr>
				        <th>Title</th>
				        <th>Uri</th>
				        <th>Template Assigned</th>
				        <th>Operations</th>
				      </tr>
				    </thead>
				    <tbody>
						<?php foreach($post_types as $key => $entity): ?>
						  <tr>
					        <td><?=$entity->title?></td>
					        <td><?=$entity->uri?></td>
					        <td><?=$entity->template_title?></td>
					        <td><a href="post_types/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="post_types/delete/<?=$entity->id?>">Delete</a></td>
					      </tr>
					    <?php endforeach ?>
				    </tbody>
				  </table>
				</div>
			</div>		
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="table-responsive">  
				  <h3>Active Theme Templates</h3>   
				  <table class="table">
				    <thead>
				      <tr>
				        <th>Title</th>
				      </tr>
				    </thead>
				    <tbody>
					<?php foreach($templates as $key => $entity): ?>
					  <tr>
				        <td><?=$entity->title?></td>
				      </tr>
				    <?php endforeach ?>
				    </tbody>
				  </table>
				</div>		
			</div>
			<div class="col-md-6">
				<div class="table-responsive"> 
					<h3>Active Theme Regions</h3>         
					  <table class="table">
					    <thead>
					      <tr>
					        <th>Title</th>
					      </tr>
					    </thead>
					    <tbody>
							<?php foreach($regions as $key => $entity): ?>
							  <tr>
						        <td><?=$entity->title?></td>
						      </tr>
						    <?php endforeach ?>
					    </tbody>
					  </table>
				 </div>				
			</div>
		</div>
</div>
<?php $this->stop() ?>

