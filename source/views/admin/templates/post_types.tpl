<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username), 'active' =>$active, 'post_types' =>$post_types]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Post Title</th>
		        <th>Post Type Prefix Uri</th>
		        <th>Template Assigned</th>
		        <th>Operations</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><?=$entity->uri?></td>
			        <td><?=$entity->template_title?></td>
			        <td><a href="post_types/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="post_types/delete/<?=$entity->id?>">Delete</a></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No Post Types Created yet. Try adding a new <a href="post_types/add">one</a></h3>
		  <?php endif; ?>
	</div>	
	<?php elseif (isset($entity)):?>
		<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/post_types/update/<?=$entity->id?>">
 			<div class="row">
		 		<div class="col-md-6">
		 		    <label>Post Type Title:</label>
		 			<input type="text" class="form-control" name="title" placeholder="Post Type Title" value="<?=$entity->title?>" required="" />
		 		</div>
				<div class="col-md-6">
			  	 	<label>Prefix Uri (This will be prefixed to any post of this post type): </label>
			 		<input type="text" class="form-control" name="uri" placeholder="Post Type uri" value="<?=$entity->uri?>" required="" />
			 	</div>
			</div>	
			<div class="row">	 		
				<div class="col-md-6">
					<label for="sel2">Select Regions:</label>
				  	<select class="form-control selectpicker" name="region_titles[]" id="sel2" multiple>
						 <?php foreach($regions as $region): ?>
							<option <?php if(strpos($entity->region_titles,$region->title)!== false){ echo 'selected';}?>><?=$region->title?></option>
					 	<?php endforeach ?>
				 	</select>		 
				</div>	
				<div class="col-md-6">
					<label for="sel1">Assign Template:</label>
				  	<select class="form-control" name="template_title" id="sel1">
				  	 <option>None</option>
					 <?php foreach($templates as $key => $template): ?>
						<option value="<?=$template->title?>" <?php if($template->title ==$entity->template_title){ echo 'selected';}?>><?=$template->title?></option>
					 <?php endforeach ?>
				 	</select>
				</div>
			</div>
			<div class="row">	
		    	<div class="col-md-12"><button class="btn btn-lg btn-primary btn-block" type="submit">Update Post Type</button></div>   
		    </div>
	 	</form> 	
	<?php else:?>
 	<form class="create" method="post" action="<?=ROOT.ADMIN_PATH?>/post_types/insert">
 	 	<div class="row">
		  	<div class="col-md-6">
		  	    <label>Post Type Title:</label>
		 		<input type="text" class="form-control" name="title" placeholder="Post Type Title" required="" />
		 	</div>
			<div class="col-md-6">
		  	 	<label>Add Prefix Uri (must be unique for each post type):</label>
		 		<input type="text" class="form-control" name="uri" placeholder="Post Type uri" required="" />
		 	</div>
		</div>
	 	<div class="row">
			<div class="col-md-6">
				<label for="sel2">Select Regions:</label>
			  	<select class="form-control selectpicker" name="region_titles[]" id="sel2" multiple>
					 <?php foreach($regions as $region): ?>
						<option><?=$region->title?></option>
				 	<?php endforeach ?>
			 	</select>		 
			</div>	
			<div class="col-md-6">
				<label for="sel1">Assign Template:</label>
			  	<select class="form-control" name="template_title" id="sel1">
			  	 <option>None</option>
				 <?php foreach($templates as $key => $template): ?>
				  	<option value="<?=$template->title?>"><?=$template->title?></option>
				 <?php endforeach ?>
			 	</select>
			</div>
		 </div>
		 <div class="row">
		  <div class="col-md-12">
		    <button class="btn btn-lg btn-primary btn-block" type="submit">Add Post Type</button>  			  
		  </div>
		 </div>
	 </form>
<?php endif; ?>
<?php $this->stop() ?>