<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username), 'active' =>$active, 'post_types' =>$post_types])?>
<?php $this->start('page')?>
<div class="row">
<h1><?=$this->e($title)?></h1>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item <?php if (!isset($_GET['mode']) or $_GET['mode']!=1){echo "active";} ?>"><a class="nav-link" data-toggle="tab" href="#gallery1" role="tab">Gallery media</a></li>
	<li class="nav-item <?php if (isset($_GET['mode'])&& $_GET['mode']==1){echo "active";} ?>"><a class="nav-link" data-toggle="tab" href="#gallery2" role="tab">Gallery thumbnails media</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane <?php if ( !isset($_GET['mode']) or $_GET['mode']!=1){echo "active";} ?>" id="gallery1" role="tabpanel">
		<div class="panel panel-default">
			<div class="row">
				<form id="media_form" name="media_form" method="post" action="<?=ROOT.ADMIN_PATH?>/media/upload" enctype="multipart/form-data">
					<div class="panel-body">
						<!-- Standar Form -->
						<h4>Select files from your computer to be uploaded in the directory: <?=$dir1?> </h4>
						<input id="input-gallery1" name="inputficons1[]" multiple type="file" class="file-loading">
						<input name="dir" value="<?=$dir1?>" type="hidden">
						<input name="id" value="<?=$gallery_id?>" type="hidden">
					</div>
				</form>
			</div>
			<div class="row">
				<div class="table-responsive">
						<?php if(isset($pages1)): ?>          
						  	<table class="table paginated">
								<thead>
									<tr>
										<th>Content</th>
										<th>Name</th>
										<th>Operations</th>
									</tr>
								</thead>
								<?php foreach($pages1 as $key => $page): ?>
								<tbody id="page1<?=$key+1?>" style="<?php if($key!=0){echo 'display: none;';} ?>">
									<?php foreach($page as $file): ?>	
									<tr>
									<td>
										<div align="left">
													<?php if($file->getExtension()=='png' or $file->getExtension()=='jpg' or $file->getExtension()=='jpeg' or $file->getExtension()=='gif'): ?><img
												src="<?=SUBFOLDER?>/<?=$file->getPathname()?>" width="220" />
													<?php elseif($file->getExtension()=='txt'): ?>
													<i class="fa fa-file-text" style="font-size: 80px;"
												aria-hidden="true"></i>
													<?php elseif($file->getExtension()=='pdf'): ?>
													<i class="fa fa-file-pdf-o" style="font-size: 80px;"
												aria-hidden="true"></i>
													<?php elseif($file->getExtension()=='xls'): ?>
													<i class="fa fa-file-excel-o" style="font-size: 80px;"
												aria-hidden="true"></i>
													<?php elseif($file->getExtension()=='word'): ?>
													<i class="fa fa-file-word-o" style="font-size: 80px;"
												aria-hidden="true"></i>
													<?php else: ?>
													<i class="fa fa-file" style="font-size: 80px;"
												aria-hidden="true"></i>
													<?php endif; ?>
													</div>
									</td>
									<td><?=$file->getFilename()?></td>
									<td><a onclick="if (! confirm('Continue?')) { return false; }"
										href='<?=ROOT.ADMIN_PATH?>/galleries/media/delete/<?=explode(".",$file->getFilename())[0]?>/<?=$file->getExtension()?>/<?=$gallery_title?>/<?=$gallery_id?>/0'><i class="fa fa-times" style="font-size: 40px;aria-hidden="true"></i>
</a></td>
									</tr>
										<?php endforeach ?>
										  </tbody>
								<?php endforeach ?>								    
							</table>
							<div class="text-center">
							 <?php if(count($pages1)>1): ?>
								  <ul class="pagination">
								  	<?php foreach($pages1 as $key => $page): ?>
								    <li class="<?php if($key==0){echo 'active';} ?>"><a href="#page1<?=$key+1?>"><?=$key+1?></a></li>
								    <?php endforeach ?>
								  </ul>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
			</div>
		</div>
	</div>
	<div class="tab-pane <?php if (isset($_GET['mode'])&& $_GET['mode']==1){echo "active";} ?>" id="gallery2" role="tabpanel">
		<div class="panel panel-default">
			<div class="row">
				<form id="media_form" name="media_form" method="post" action="<?=ROOT.ADMIN_PATH?>/media/upload" enctype="multipart/form-data">
					<div class="panel-body">
						<!-- Standar Form -->
						<h4>Select files from your computer to be uploaded in the directory: <?=$dir2?> </h4>
						<input id="input-gallery2" name="inputficons2[]" multiple type="file" class="file-loading">
						<input name="dir" value="<?=$dir2?>" type="hidden">
						<input name="id" value="<?=$gallery_id?>" type="hidden">
					</div>
				</form>
			</div>
			<div class="row">
				<div class="table-responsive">
							<?php if(isset($pages2)): ?>          
						  	<table class="table paginated">
							<thead>
								<tr>
									<th>Content</th>
									<th>Name</th>
									<th>Operations</th>
								</tr>
							</thead>
							<?php foreach($pages2 as $key => $page): ?>
							<tbody id="page2<?=$key+1?>" style="<?php if($key!=0){echo 'display: none;';} ?>">
								<?php foreach($page as $file): ?>	
								<tr>
								<td>
									<div align="left">
												<?php if($file->getExtension()=='png' or $file->getExtension()=='jpg' or $file->getExtension()=='jpeg' or $file->getExtension()=='gif'): ?><img
											src="<?=SUBFOLDER?>/<?=$file->getPathname()?>" width="220" />
												<?php elseif($file->getExtension()=='txt'): ?>
												<i class="fa fa-file-text" style="font-size: 80px;"
											aria-hidden="true"></i>
												<?php elseif($file->getExtension()=='pdf'): ?>
												<i class="fa fa-file-pdf-o" style="font-size: 80px;"
											aria-hidden="true"></i>
												<?php elseif($file->getExtension()=='xls'): ?>
												<i class="fa fa-file-excel-o" style="font-size: 80px;"
											aria-hidden="true"></i>
												<?php elseif($file->getExtension()=='word'): ?>
												<i class="fa fa-file-word-o" style="font-size: 80px;"
											aria-hidden="true"></i>
												<?php else: ?>
												<i class="fa fa-file" style="font-size: 80px;"
											aria-hidden="true"></i>
												<?php endif; ?>
												</div>
								</td>
								<td><?=$file->getFilename()?></td>
								<td><a onclick="if (! confirm('Continue?')) { return false; }"
									href='<?=ROOT.ADMIN_PATH?>/galleries/media/delete/<?=explode(".",$file->getFilename())[0]?>/<?=$file->getExtension()?>/<?=$gallery_title?>/<?=$gallery_id?>/1'><i class="fa fa-times"  style="font-size: 40px;" aria-hidden="true"></i>
</a></td>
							</tr>
									    <?php endforeach ?>
									  </tbody>
								    <?php endforeach ?>								    
						</table>
						<?php endif; ?>
						<div class="text-center">
							 <?php if(count($pages2)>1): ?>
								  <ul class="pagination">
								  	<?php foreach($pages2 as $key => $page): ?>
								    <li class="<?php if($key==0){echo 'active';} ?>"><a href="#page2<?=$key+1?>"><?=$key+1?></a></li>
								    <?php endforeach ?>
								  </ul>
							<?php endif; ?>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->stop() ?>