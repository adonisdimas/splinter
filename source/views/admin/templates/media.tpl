<?php
if (isset ( $box ) && $box == 1) {
	$this->layout ( 'layout', [ 
			'title' => $this->e ( $title ),
			'locale' => $this->e ( $locale ) 
	] );
	$this->start ( 'main-content' );
} else {
	$this->layout ( 'base', [ 
			'title' => $this->e ( $title ),
			'locale' => $this->e ( $locale ),
			'username' => $this->e ( $username ),
			'active' => $active 
	] );
	$this->start ( 'page' );
}
?>
<div class="row">
	<h1><?=$this->e($title)?></h1>
	<div class="row">
		<form id="media_form" name="media_form" method="post" action="media/upload" enctype="multipart/form-data">
			<div class="panel-body">
				<!-- Standar Form -->
				<h4>Select files from your computer</h4>
				<input id="input-ficons-1" name="inputficons1[]" multiple type="file" class="file-loading">
				<input name="dir" value="<?=MEDIA_PATH?>" type="hidden">
				<input name="redir" value="<?php if(isset($box)){echo '/media_box';}else{echo '/media';} ?>" type="hidden">
			</div>
		</form>
	</div>
	<div class="row">
		<div class="table-responsive">
					<?php if(isset($pages)): ?>          
				  	<table class="table paginated">
					<thead>
						<tr>
							<th>Content</th>
							<th>Type</th>
							<th>Path</th>
							<th>Operations</th>
						</tr>
					</thead>
					<?php foreach($pages as $key => $page): ?>
					<tbody id="page<?=$key+1?>" style="<?php if($key!=0){echo 'display: none;';} ?>">
						<?php foreach($page as $file): ?>	
						<tr>
						<td>
							<div align="left">
										<?php if($file->getExtension()=='png' or $file->getExtension()=='jpg' or $file->getExtension()=='jpeg' or $file->getExtension()=='gif'): ?><img
									src="<?=SUBFOLDER?>/<?=$file->getPathname()?>" width="150" />
										<?php elseif($file->getExtension()=='txt'): ?>
										<i class="fa fa-file-text" style="font-size: 100px;"
									aria-hidden="true"></i>
										<?php elseif($file->getExtension()=='pdf'): ?>
										<i class="fa fa-file-pdf-o" style="font-size: 100px;"
									aria-hidden="true"></i>
										<?php elseif($file->getExtension()=='xls'): ?>
										<i class="fa fa-file-excel-o" style="font-size: 100px;"
									aria-hidden="true"></i>
										<?php elseif($file->getExtension()=='word'): ?>
										<i class="fa fa-file-word-o" style="font-size: 100px;"
									aria-hidden="true"></i>
										<?php else: ?>
										<i class="fa fa-file" style="font-size: 100px;"
									aria-hidden="true"></i>
										<?php endif; ?>
										</div>
						</td>
						<td><?=$file->getExtension()?></td>
						<td><input class="selectOnClick" type="text" readonly="readonly"
							value="<?=SUBFOLDER?>/<?=$file->getPathname()?>" /></td>
						<td><a onclick="if (! confirm('Continue?')) { return false; }"
							href='<?php if(isset($box)){echo 'media_box/delete/';}else{echo 'media/delete/';} ?><?=explode(".",$file->getFilename())[0]?>/<?=$file->getExtension()?>'>Delete</a></td>
					</tr>
							    <?php endforeach ?>
							  </tbody>
						    <?php endforeach ?>								    
				</table>
			<div class="text-center">
						<?php if(count($pages)>1): ?>
						  <ul class="pagination">
						  	<?php foreach($pages as $key => $page): ?>
						    <li class="<?php if($key==0){echo 'active';} ?>"><a
						href="#page<?=$key+1?>"><?=$key+1?></a></li>
						    <?php endforeach ?>
						  </ul>
					  <?php endif; ?>
				  </div>
				  <?php else: ?>
				  	<h2>
				No Filed Added in the Media library. Try adding <a href="#upload">here</a>
			</h2>
				  <?php endif; ?>
			</div>
	</div>
</div>
<!-- /container -->
<?php $this->stop() ?>