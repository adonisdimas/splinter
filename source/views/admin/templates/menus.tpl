<?php $this->layout('base', ['title' => $this->e($title),'locale' => $this->e($locale),'username' => $this->e($username), 'active' =>$active, 'post_types' =>$post_types]) ?>
<?php $this->start('page') ?>
<h1><?=$this->e($title)?></h1>
<?php if(isset($entities)): ?>
	<?php if(count($entities)>0): ?>
	<div class="table-responsive">          
		  <table class="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Classes</th>
		        <th>Order</th>
		        <th>Region Assigned</th>
		        <th>Operations</th>
		      </tr>
		    </thead>
		    <tbody>
				<?php foreach($entities as $key => $entity): ?>
				  <tr>
			        <td><?=$entity->title?></td>
			        <td><?=$entity->classes?></td>
			        <td><?=$entity->order?></td>
			        <td><?=$entity->region_title?></td>
			        <td><a href="<?=ROOT.ADMIN_PATH?>/menu_items/edit/<?=$entity->id?>">Add/Edit Menu Items</a> | <a href="menus/edit/<?=$entity->id?>">Edit</a> | <a onclick = "if (! confirm('Continue?')) { return false; }" href="menus/delete/<?=$entity->id?>">Delete</a></td>
			      </tr>
			    <?php endforeach ?>
		    </tbody>
		  </table>
		  <?php else: ?>
		  	<h3>No Menus Created yet. Try adding a new <a href="menus/add">one</a></h3>
		  <?php endif; ?>
	</div>	
	<?php elseif (isset($entity)):?>
		<form class="update" method="post" action="<?=ROOT.ADMIN_PATH?>/menus/update/<?=$entity->id?>">
		     <div class="row">
			     <div class="col-md-6"> 
			     	<input type="text" class="form-control" name="title" placeholder="Menu Title" value="<?=$entity->title?>" required="" />
			     </div>
			     <div class="col-md-6"> 	  	
	 	  			<input type="text" class="form-control" name="classes" placeholder="Menu Classes" value="<?=$entity->classes?>" />		
				</div>
			</div>
			<div class="row">
			 	<div class="col-md-6"> 
					<label for="sel1">Assign Region:</label>
					<select class="form-control" name="region_title" id="sel1">
						<option>None</option>
						<?php foreach($regions as $key => $region): ?>
							<option value="<?=$region->title?>" <?php if($region->title ==$entity->region_title){ echo 'selected';}?>><?=$region->title?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-6"> 
					<label for="sel2">Assign Order(The smaller the higher):</label>
					<select class="form-control" name="order" id="sel2">
						<?php $numbs = array(0,1,2,3,4,5,6,7,8,9,10); foreach($numbs as $num): ?>
						 	<option value="<?=$num?>" <?php if($num ==$entity->order){ echo 'selected';}?>><?=$num?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
	     <button class="btn btn-lg btn-primary btn-block" type="submit">Update Menu</button>   
	 	</form> 	
	<?php elseif (isset($menu_items)):?>
  		 <form class="create" method="post" action="<?=ROOT.ADMIN_PATH?>/menu_items/insert">
		 	 	<div class="row">
				  <div class="col-md-12">
			 		<ul class="nav nav-tabs" role="tablist">
			 		<?php foreach($languages as $key => $language): ?>
			 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
						<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
					</li>
		 			<?php endforeach ?>
		 			</ul>
					<div class="tab-content">
		 			<?php foreach($languages as $key => $language): ?>
		 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
				     	<div class="auto-elements">
				     		<div class="elements sortable">
				     		<?php if(count($menu_items[$key])<=0): ?>
					      	<div class="row after-add-more added-element grabbable">
					         	<div class="col-md-4">
						            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][title]" placeholder="Title" required/>
						        </div>
						        <div class="col-md-4">
						            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][link]" placeholder="Link" />
						        </div>
						        <div class="col-md-4">
									<select class="form-control" name="menu_item[<?=$language->id?>][][target]">
										<option>None</option>
										<option value="_blank">Blank</option>
										<option value="_self">Self</option>
										<option value="_parent">Parent</option>
										<option value="_top">Top</option>												
									</select>
						        </div>
					        </div>					     		
								<?php else:?>
					     		<?php foreach($menu_items[$key] as $index => $menu_item): ?>
						      	<div class="row after-add-more added-element grabbable">
						         	<div class="col-md-4">
							            <input type="text" class="form-control" value="<?=$menu_item->title?>" name="menu_item[<?=$language->id?>][][title]" placeholder="Title" />
							        </div>
							        <div class="col-md-4">
							            <input type="text" class="form-control" value="<?=$menu_item->link?>" name="menu_item[<?=$language->id?>][][link]" placeholder="Link" />
							        </div>
							        <div class="col-md-4">
										<select class="form-control" name="menu_item[<?=$language->id?>][][target]">
											<option>None</option>
											<option value="_blank" <?php if($menu_item->target == '_blank'){ echo 'selected';}?>>Blank</option>
											<option value="_self" <?php if($menu_item->target == '_self'){ echo 'selected';}?>>Self</option>
											<option value="_parent" <?php if($menu_item->target == '_parent'){ echo 'selected';}?>>Parent</option>
											<option value="_top" <?php if($menu_item->target == '_top'){ echo 'selected';}?>>Top</option>												
										</select>
							        </div>								               
						        </div>
						       <?php endforeach ?>									
								<?php endif; ?>
						   </div>
					        <!-- Copy Fields -->
					        <div class="copy hide">
					          <div class="row added-element grabbable" style="margin:5px 0">
						         	<div class="col-md-4">
							            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][title]" placeholder="Title"/>
							        </div>
							        <div class="col-md-4">
							            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][link]" placeholder="Link" />
							        </div>	
							        <div class="col-md-4">
										<select class="form-control" name="menu_item[<?=$language->id?>][][target]">
											<option>None</option>
											<option value="_blank">Blank</option>
											<option value="_self">Self</option>
											<option value="_parent">Parent</option>
											<option value="_top">Top</option>												
										</select>
							        </div>           
					          </div>
					        </div>
				     	</div>
				     	<div class="input-group-btn"> 
							<button class="btn btn-success add-btn" type="button"><i class="glyphicon glyphicon-plus"></i> Add Menu Item</button>
							<button class="btn btn-danger remove-btn" type="button"><i class="glyphicon glyphicon-minus"></i> Remove Menu Item</button>
						</div>
		 			</div>
		 			<?php endforeach ?>
					</div>
					</br>
		    		<button class="btn btn-lg btn-primary btn-block" type="submit">Update Menu Items</button>  	
		    		<input type="hidden" class="form-control" name="menu_id" value="<?=$menu_id?>" /> 
			  </div>
			</div> 
		</form>
	<?php else:?>
 	<form class="create" method="post" action="insert">
 	 	<div class="row">
		    <div class="row">
			     <div class="col-md-6"> 
			     	<input type="text" class="form-control" name="title" placeholder="Menu Title" required="" />
			     </div>
			     <div class="col-md-6"> 	  	
	 	  			<input type="text" class="form-control" name="classes" placeholder="Menu Classes" />		
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<label for="sel1">Assign Region:</label>
					  <select class="form-control" name="region_title" id="sel1">
					  	<option>None</option>
					  	<?php foreach($regions as $key => $region): ?>
				    	<option value="<?=$region->title?>"><?=$region->title?></option>
				    <?php endforeach ?>
				  </select>
			  	</div>
			  	<div class="col-md-6">
				<label for="sel2">Assign Order(The smaller the higher):</label>
				  <select class="form-control" name="order" id="sel2">
				  	<?php $numbs = array(0,1,2,3,4,5,6,7,8,9,10);
				  	foreach($numbs as $num): ?>
				    	<option value="<?=$num?>"><?=$num?></option>
				    <?php endforeach ?>
				  </select>		  
			  	</div>
		  	</div>
		  <div class="col-md-12">
	 		<ul class="nav nav-tabs" role="tablist">
	 		<?php foreach($languages as $key => $language): ?>
		 		<li class="nav-item <?php if($key ==0){ echo 'active';}?>">
					<a class="nav-link" data-toggle="tab" href="#<?=$language->locale?>" role="tab"><?=$language->title?></a>
				</li>
	 		<?php endforeach ?>
	 		</ul>
			<div class="tab-content">
	 		<?php foreach($languages as $key => $language): ?>
	 			<div class="tab-pane <?php if($key ==0){ echo 'active';}?>" id="<?=$language->locale?>" role="tabpanel">
			     	<div class="auto-elements">
				      	<div class="elements sortable">
					      	<div class="row after-add-more added-element grabbable">
					         	<div class="col-md-4">
						            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][title]" placeholder="Title" required/>
						        </div>
						        <div class="col-md-4">
						            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][link]" placeholder="Link" />
						        </div>
						        <div class="col-md-4">
											<select class="form-control" name="menu_item[<?=$language->id?>][][target]">
												<option>None</option>
												<option value="_blank">Blank</option>
												<option value="_self">Self</option>
												<option value="_parent">Parent</option>
												<option value="_top">Top</option>												
											</select>
						        </div>
					        </div>
				        </div>
				        <!-- Copy Fields -->
				        <div class="copy hide">
				          <div class="row added-element grabbable" style="margin:5px 0">
				         	<div class="col-md-4">
					            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][title]" placeholder="Title"/>
					        </div>
					        <div class="col-md-4">
					            <input type="text" class="form-control" name="menu_item[<?=$language->id?>][][link]" placeholder="Link" />
					        </div>	
					        <div class="col-md-4">
										<select class="form-control" name="menu_item[<?=$language->id?>][][target]">
											<option>None</option>
											<option value="_blank">Blank</option>
											<option value="_self">Self</option>
											<option value="_parent">Parent</option>
											<option value="_top">Top</option>												
										</select>
					        </div>           
				          </div>
				        </div>
			     	</div>
			     	<div class="input-group-btn"> 
							<button class="btn btn-success add-btn" type="button"><i class="glyphicon glyphicon-plus"></i> Add Menu Item</button>
							<button class="btn btn-danger remove-btn" type="button"><i class="glyphicon glyphicon-minus"></i> Remove Menu Item</button>
						</div>
	 			</div>
	 		<?php endforeach ?>
			</div>
			</br>
		    <button class="btn btn-lg btn-primary btn-block" type="submit">Add Menu</button>  			  
		  </div>
	</div> 
</form>
<?php endif; ?>
<?php $this->stop() ?>