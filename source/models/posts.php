<?php
namespace Entity;
use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Post_Type extends \Spot\Entity
{
    protected static $table = 'post_types';
    
    public function get_table() {
    	return self::$table;
    }
    public static function fields()
    {
        return [
            'id'              => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'title'           => ['type' => 'string', 'required' => true],
        	'uri'          	  => ['type' => 'string', 'required' => true, 'unique' => true],
        	'region_titles'   => ['type' => 'string', 'required' => false],
        	'template_title'  => ['type' => 'string', 'required' => false]
        ];
    }
    public function insert_default($db)
    {
    	$mapper=$db->mapper('Entity\Post_Type'); 
    	$mapper->insert([
    			'id' => 1,
    			'title' => 'blog',
    			'uri' =>'/blog/',
    			'region_titles'   => 'header,main,footer',
    			'template_title'  => 'default'
    	]);
    }
}
class Post extends \Spot\Entity
{
	protected static $table = 'posts';
	protected static $mapper = 'Entity\Mapper\Post';
	
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           			=> ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'post_type_id'         	=> ['type' => 'integer', 'required' => true],
				'lang_id'         		=> ['type' => 'integer', 'required' => true],
				'uri'          	  		=> ['type' => 'string', 'required' => true, 'unique' => true],
				'title'         		=> ['type' => 'text', 'required' => true],
				'content'         		=> ['type' => 'text', 'default' =>' ', 'required' => true]
		];
	}
	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'id' => $mapper->belongsTo($entity, 'Entity\Post_type', 'post_type_id'),
				'id' => $mapper->belongsTo($entity, 'Entity\Language', 'lang_id')
		];
	}
	public function insert_default($db)
	{
		$mapper=$db->mapper('Entity\Post');
	}
}

namespace Entity\Mapper;
use Spot\Mapper;

class Post extends Mapper
{
	public function get_post_translation($lang_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$results = (array)$this->query("SELECT * FROM `posts` WHERE `id` = ".$post_id." and `lang_id` = ".$lang_id."");
		return $results[chr(0).'*'.chr(0).'results'];
	}
	public function get_translated_posts_by_type($post_type_id,$lang_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$results = (array)$this->query("SELECT * FROM `posts` WHERE `post_type_id` = ".$post_id." and `lang_id` = ".$lang_id."");
		return $results[chr(0).'*'.chr(0).'results'];
	}
}

?>