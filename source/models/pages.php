<?php
namespace Entity;
use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Page extends \Spot\Entity
{
    protected static $table = 'pages';
    
    public function get_table() {
    	return self::$table;
    }
    public static function fields()
    {
        return [
            'id'              => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'title'           => ['type' => 'string', 'required' => true],
        	'uri'          	  => ['type' => 'string', 'required' => true, 'unique' => true],
        	'region_titles'   => ['type' => 'string', 'required' => false],
        	'template_title'  => ['type' => 'string', 'required' => false]
        ];
    }
    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
        	'page_translation_id' => $mapper->hasOne($entity, 'Entity\Page_Translation', 'page_id'),
        ];
    }
    public function insert_default($db)
    {
    	$mapper=$db->mapper('Entity\Page'); 
    }
}
class Page_Translation extends \Spot\Entity
{
	protected static $table = 'pages_translation';
	protected static $mapper = 'Entity\Mapper\Page_Translation';
	
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           			=> ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'page_id'         		=> ['type' => 'integer', 'required' => true],
				'lang_id'         		=> ['type' => 'integer', 'required' => true],
				'meta_title'        	=> ['type' => 'string', 'required' => false],
				'meta_description' 	 	=> ['type' => 'string', 'required' => false],
				'meta_keywords'     	=> ['type' => 'string', 'required' => false],
				'title'         		=> ['type' => 'text', 'required' => true],
		];
	}
	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'id' => $mapper->belongsTo($entity, 'Entity\Page', 'page_id'),
				'id' => $mapper->belongsTo($entity, 'Entity\Language', 'lang_id')
		];
	}
	public function insert_default($db)
	{
		$mapper=$db->mapper('Entity\Page_Translation');
	}
}
class Page_Content extends \Spot\Entity
{
	protected static $table = 'pages_content';
	protected static $mapper = 'Entity\Mapper\Page_Content';
	
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           					=> ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'page_translation_id'   		=> ['type' => 'integer', 'required' => true],
				'page_id'         				=> ['type' => 'integer', 'required' => true],
				'lang_id'         				=> ['type' => 'integer', 'required' => true],
				'content'         				=> ['type' => 'text', 'default' =>' ', 'required' => true],
		];
	}

	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'id' => $mapper->belongsTo($entity, 'Entity\Page', 'page_id'),
				'id' => $mapper->belongsTo($entity, 'Entity\Page_Translation', 'page_translation_id'),
				'id' => $mapper->belongsTo($entity, 'Entity\Language', 'lang_id')
		];
	}
	public function insert_default($db)
	{
		$mapper=$db->mapper('Entity\Page_Content');		
	}
}
class Template extends \Spot\Entity
{
	protected static $table = 'templates';
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
				'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
				'title'        => ['type' => 'string', 'required' => true]
		];
	}
	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'page_id' => $mapper->hasOne($entity, 'Entity\Page', 'template_id')
		];
	}
}
namespace Entity\Mapper;
use Spot\Mapper;

class Page_Translation extends Mapper
{
	public function get_page_translation($page_id,$lang_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$results = (array)$this->query("SELECT * FROM `pages_translation` WHERE `page_id` = ".$page_id." and `lang_id` = ".$lang_id."");
		return $results[chr(0).'*'.chr(0).'results'];
	}
}
class Page_Content extends Mapper
{
	public function get_page_content($page_id,$lang_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$results = (array)$this->query("SELECT * FROM `pages_content` WHERE `page_id` = ".$page_id." and `lang_id` = ".$lang_id."");
		return $results[chr(0).'*'.chr(0).'results'];
	}
}
?>