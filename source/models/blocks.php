<?php
namespace Entity;
use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Block extends \Spot\Entity
{
    protected static $table = 'blocks';
    
    public function get_table() {
    	return self::$table;
    }
    public static function fields()
    {
        return [
            'id'           => ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
            'title'        => ['type' => 'string', 'required' => true],
        	'classes'      => ['type' => 'string', 'required' => false],
        	'order'        => ['type' => 'integer', 'default' => 0, 'required' => false],
        	'region_title' => ['type' => 'string', 'required' => false]
        ];
    }
    public function insert_default($db)
    {
    	$mapper=$db->mapper('Entity\Block'); 	
    }
}
class Block_Content extends \Spot\Entity
{
	protected static $table = 'blocks_content';
	protected static $mapper = 'Entity\Mapper\Block_Content';
	
	public function get_table() {
		return self::$table;
	}
	public static function fields()
	{
		return [
			'id'          				=> ['type' => 'integer', 'autoincrement' => true, 'primary' => true],
			'block_id'  				=> ['type' => 'integer', 'required' => true],
			'lang_id'         			=> ['type' => 'integer', 'required' => true],
			'hide_title'    			=> ['type' => 'integer','default' => 0],
			'title'         			=> ['type' => 'text', 'required' => true],
			'content'         			=> ['type' => 'text', 'required' => true],
		];
	}

	public static function relations(Mapper $mapper, Entity $entity)
	{
		return [
				'id' => $mapper->belongsTo($entity, 'Entity\Block', 'block_id'),
				'id' => $mapper->belongsTo($entity, 'Entity\Language', 'lang_id')
		];
	}
	public function insert_default($db)
	{
		$mapper=$db->mapper('Entity\Block_Content');		
	}
}
namespace Entity\Mapper;
use Spot\Mapper;

class Block_Content extends Mapper
{
	public function all_translated_blocks($block_id)
	{
		//Custom way to retrieve protected results for this collection that does not have a getter method
		$blck_contents = (array)$this->query("SELECT * FROM `blocks_content` where `block_id` = ".$block_id."");
		return $blck_contents[chr(0).'*'.chr(0).'results'];
	}
}
?>