<?php
define('DEFAULT_LOCALE', 'en');
define('DEFAULT_THEME', 'default');
define('SUBFOLDER', '/splinter/source');/*ADD THE RELATIVE PATH OF THE SUBFOLDER THE CMS IS LOCATED*/
define('ROOT', get_server_protocol().$_SERVER['HTTP_HOST'].SUBFOLDER);  /*ROOT*/
define('URI', rawurldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));  /*ROOT*/
/*****MVC PATH CONFIGURATION*****/
define('CONTROLLERS_PATH', 'controllers');
define('MODELS_PATH', 'models');
define('VIEWS_PATH', 'views');
define('VENDOR_PATH', 'vendor');
define('ADMIN_PATH', '/admin');
define('VIEWS_ADMIN_PATH', VIEWS_PATH.'/admin');
define('ADMIN_ASSETS', SUBFOLDER.'/'.VIEWS_ADMIN_PATH.'/assets');
define('MEDIA_PATH', VIEWS_PATH.'/media');
define('GALLERIES_PATH', MEDIA_PATH.'/galleries');
define('GALLERIES_THUMBS_DIR','/thumbs');
define('SLIDERS_PATH', MEDIA_PATH.'/sliders');
define('VIEWS_THEME_PATH', VIEWS_PATH.'/themes');
/*****DATABASE CONFIGURATION*****/
define('DB_NAME', 'splinter_cms_db');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_DRIVER', 'pdo_mysql');
/*****WEBSITE SETTINGS*****/
$GLOBALS['active_theme']  = DEFAULT_THEME;
$GLOBALS['default_locale']  = DEFAULT_LOCALE;
$GLOBALS['available_themes']  = get_dir_folder_names(VIEWS_THEME_PATH);
?>