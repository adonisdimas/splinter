<?php
namespace Theme;
use League\Plates\Engine;
use League\Plates\Extension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Controller {
	public $templates;
	public $locale;
	public $db;
	function __construct() {
		$this->db = db_connect ();
		$mapper = $this->db->mapper ( 'Entity\Setting' );
		$setting = $mapper->where ( ['title' => 'Active Theme'] )->first ();
		$default_language = $mapper->where ( ['Title' => 'Default Language'] )->first ();
		$this->default_locale = $default_language->value;
		$this->locale = get_locale (rawurldecode ( parse_url ( $_SERVER ['REQUEST_URI'], PHP_URL_PATH ) ),$this->default_locale);
		$this->active_theme = $setting->value;
		$this->active_theme_assets_path = VIEWS_THEME_PATH.'/'.$this->active_theme.'/assets';
		$this->theme_assets = $this->get_assets($this->active_theme_assets_path);
		$this->slider_assets = ['scripts' => array(),'styles' => array()];
		$this->gallery_assets = ['scripts' => array(),'styles' => array()];
		$this->templates = new Engine ( VIEWS_THEME_PATH.'/'.$this->active_theme, 'tpl' );
		$this->templates->loadExtension ( new Extension\URI ( $_SERVER ['PATH_INFO'] ) );
	}
	public function not_found(Request $request, Response $response) {
		$response->setContent ( $this->templates->render ( 'not-found', [
				'title' => 'Page Not Found',
				'locale' => $this->locale,
				'theme_assets_path' => $this->active_theme_assets_path
		] ) );
		return $response;		
	}
	public function action(Request $request, Response $response) {
		$uri = get_parsed_uri();
		$mapper = $this->db->mapper ( 'Entity\Language' );
		$language = $mapper->where ( ['locale' => $this->locale] )->first ();
		if (!$language) {
			$language = $mapper->where ( ['locale' => $this->default_locale] )->first ();
		}
		$mapper = $this->db->mapper ( 'Entity\Page' );
		$page = $mapper->where ( ['uri' => $uri] )->first ();
		$mapper = $this->db->mapper ( 'Entity\Template' );
		$page_template = $mapper->where ( ['title' => $page->template_title ] )->first ();
		$mapper = $this->db->mapper ('Entity\Page_Translation' );
		$page_translation = $mapper->where (['page_id' => $page->id] )->first ();
		$mapper = $this->db->mapper ( 'Entity\Page_Content' );
		$page_contents = $mapper->where ( [ 
				'page_translation_id' => $page_translation->id,
				'lang_id' => $language->id 
		] );
		$selected_regions = explode(',',$page->region_titles);
		$regions = $this->get_regions($selected_regions,$language);
		$sliders = $this->get_sliders($page,$language);
		$galleries = $this->get_galleries($page);
		// Check if template name exists in the theme folder
		if($page_template && $this->templates->exists ( 'templates/' . $page_template->title )) {
			$template = $page_template->title;
		} else {
			$template = 'default';
		}
		$mapper = $this->db->mapper ( 'Entity\Translation' );
		$translations_all_page = $mapper->where ( ['lang_id' => $language->id,'region_title' =>NULL]);
		$translations_page=array();
		foreach ($translations_all_page as $key => $translation ) {
			$translations_page[$translation->title] = $translation->value;
		}
		$mapper = $this->db->mapper ( 'Entity\Setting' );
		$settings_all = $mapper->all ();
		$settings=array();
		foreach ($settings_all as $key => $setting ) {
			$settings[$setting->title] = $setting->value;
		}
		$response->setContent ( $this->templates->render ( 'templates/' . $template, [ 
				'page_title' => $page->title,
				'page_translation' => $page_translation, 
				'page_contents' => $page_contents,
				'regions' => $regions,
				'sliders' => $sliders,
				'sliders_assets' => $this->slider_assets,
				'sliders_path' => '../../../'.SLIDERS_PATH,
				'galleries' => $galleries,
				'galleries_assets' => $this->gallery_assets,
				'galleries_path' => '../../../'.GALLERIES_PATH,
				'theme_assets' => $this->theme_assets,
				'theme_assets_path' => $this->active_theme_assets_path,
				'translations' => $translations_page,
				'settings' => $settings,
				'locale' => $this->locale
		] ) );
		return $response;
	}
	public function get_assets($path){
		$assets;
		$assets['styles'] = get_stylesheets_all($path.'/css');
		$assets['scripts'] = get_scripts_all($path.'/js');
		return $assets;
	}
	public function get_galleries($page){
		$mapper = $this->db->mapper ( 'Entity\Gallery' );
		$available_galleries = $mapper->all ();
		$galleries=array();
		$galleries_pages =array();
		foreach ($available_galleries as $key => $gallery ) {
			$pages = explode(',',$gallery->page_titles);
			foreach ($pages as $page1 ) {
				if($page->title == $page1){
					$files['images'] = get_dir_files('/'.GALLERIES_PATH.'/'.$gallery->title);
					$files['thumbnails'] = get_dir_files('/'.GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR);
					$galleries['pages'][] = [$gallery,$files];
					$this->append_gallery_assets($gallery->type);
				}
			}
		}
		return $galleries;
	}
	public function get_sliders($page,$language){
		$mapper = $this->db->mapper ( 'Entity\Slider' );
		$available_sliders = $mapper->all ();
		$sliders=array();
		$slider_pages =array();
		foreach ($available_sliders as $key => $slider ) {
			//assign all sliders to the current page (if selected)
			$pages = explode(',',$slider->page_titles);
			$mapper = $this->db->mapper ( 'Entity\Slider_Item' );
			foreach ($pages as $page1 ) {
				if($page->title == $page1){
					$slider_pages[] = [
						$slider,
						$mapper->where ( [
								'slider_id' => $slider->id,
								'lang_id' => $language->id
						] )
					];
					$this->append_slider_assets($slider->type);
				}
			}
			$sliders['pages'] = $slider_pages;
		}
		return $sliders;
	}
	public function append_gallery_assets($type){
		if($type == "masonry"){
			$this->gallery_assets['scripts'] = append_script($this->gallery_assets['scripts'],GALLERIES_PATH.'/assets/js/masonry.min.js');
		}else if($type == "carousel"){
			$this->gallery_assets['scripts'] = append_script($this->gallery_assets['scripts'],GALLERIES_PATH.'/assets/js/owl.carousel.min.js');
			$this->gallery_assets['styles'] = append_style($this->gallery_assets['styles'],GALLERIES_PATH.'/assets/css/owl.carousel.min.css');
		}else{
			$this->gallery_assets['scripts'] = append_script($this->gallery_assets['scripts'],GALLERIES_PATH.'/assets/js/lightgallery.min.js');
			$this->gallery_assets['styles'] = append_style($this->gallery_assets['styles'],GALLERIES_PATH.'/assets/css/lightgallery.min.css');
			$this->gallery_assets['styles'] = append_style($this->gallery_assets['styles'],GALLERIES_PATH.'/assets/css/lg-transitions.min.css');
		}
	}
	public function append_slider_assets($type){
		if($type == "fotorama"){
			$this->slider_assets['scripts'] = append_script($this->slider_assets['scripts'],SLIDERS_PATH.'/assets/js/fotorama.min.js');
			$this->slider_assets['styles'] = append_style($this->slider_assets['styles'],SLIDERS_PATH.'/assets/css/fotorama.min.css');
		}else if($type == "bxslider"){
			$this->slider_assets['scripts'] = append_script($this->slider_assets['scripts'],SLIDERS_PATH.'/assets/js/bxslider.min.js');
			$this->slider_assets['styles'] = append_style($this->slider_assets['styles'],SLIDERS_PATH.'/assets/css/bxslider.min.css');		
		}else{
			$this->slider_assets['scripts'] = append_script($this->slider_assets['scripts'],SLIDERS_PATH.'/assets/js/lightslider.min.js');
			$this->slider_assets['styles'] = append_style($this->slider_assets['styles'],SLIDERS_PATH.'/assets/css/lightslider.min.css');		
		}
	}
	public function get_regions($selected_regions,$language){
		// Assign translated menus and blocks to the respective regions
		$mapper = $this->db->mapper ( 'Entity\Menu' );
		$available_menus = $mapper->all ();
		$mapper = $this->db->mapper ( 'Entity\Block' );
		$available_blocks = $mapper->all ();
		$mapper = $this->db->mapper ( 'Entity\Translation' );
		$available_translations = $mapper->all ();
		$mapper = $this->db->mapper ( 'Entity\Gallery' );
		$available_galleries = $mapper->all();
		$regions=array();
		$region_blocks=array();
		$region_menus =array();
		$region_translations =array();
		foreach ( $selected_regions as $key => $region ) {
			$mapper = $this->db->mapper ( 'Entity\Menu_Item' );
			foreach ( $available_menus as $menu ) {
				if ($menu->region_title == $region) {
					$region_menus[] = [
							'order' => $menu->order,'contents' =>[
								$menu->classes,
								$mapper->where ( ['menu_id' => $menu->id,'lang_id' => $language->id] )
							]							
					];
				}
			}
			usort($region_menus,'sort_by_order_asc');
			$mapper = $this->db->mapper ( 'Entity\Block_Content' );
			foreach ( $available_blocks as $block ) {
				if ($block->region_title == $region) {
					$region_blocks[] =[
							'order' => $block->order ,'contents' =>[
								$block->classes,
								$mapper->where ( ['block_id' => $block->id,'lang_id' => $language->id] )									
							]
					];
				}
			}
			usort($region_blocks,'sort_by_order_asc');
			$mapper = $this->db->mapper ( 'Entity\Translation' );
			foreach ( $available_translations as $translation ) {
				if ($translation->region_title == $region) {
					$region_translations[$translation->title] = $translation->value;
				}
			}
			$regions[$region]['blocks'] = $region_blocks;
			$regions[$region]['menus'] = $region_menus;
			$regions[$region]['translations'] = $region_translations;
			$region_blocks=array();
			$region_menus =array();
			$region_translations =array();
		}
		return $regions;
	}
}
?>