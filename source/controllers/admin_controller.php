<?php
namespace Admin;
use League\Plates\Engine;
use League\Plates\Extension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use PHPMailer;

class Controller {
	public $templates;
	public $locale;
	public $db;
	public $user;
	public $router;
	function __construct() {
		$this->templates = new Engine ( VIEWS_ADMIN_PATH, 'tpl' );
		$this->templates->loadExtension ( new Extension\URI ( $_SERVER ['REQUEST_URI'] ) );
		$this->locale = get_locale (URI);
		$this->db = db_connect ();
		$this->uri = get_parsed_uri();
	}
	public function login(Request $request, Response $response) {
		$response->setContent ( $this->templates->render ( 'templates/login', [ 
				'title' => 'login',
				'locale' => $this->locale 
		] ) );
		return $response;
	}
	public function authenticate(Request $request, Response $response) {
		$mapper = $this->db->mapper ( 'Entity\User' );
		$this->user = $mapper->first ( ['username' => $request->request->get ( 'username' )] );
		if (password_verify ($request->request->get ( 'password' ), $this->user->password )) {
			session_init ( $request->request->get ( 'username' ) );
			redirect ( ADMIN_PATH.'/dashboard' );
		} else {
			redirect ( ADMIN_PATH.'/login?error' );
		}
		return $response;
	}
	public function logout(Request $request, Response $response) {
		logout ();
		redirect ( ADMIN_PATH.'/login' );
		return $response;
	}
	public function dashboard(Request $request, Response $response) {
		if (session_check ()) {
			$response->setContent ( $this->templates->render ( 'templates/dashboard', [ 
					'title' => 'Dashboard',
					'regions'=>$this->db->mapper ( 'Entity\Region' )->all(),
					'templates'=>$this->db->mapper ( 'Entity\Template' )->all(),
					'users'=>$this->db->mapper ( 'Entity\User' )->all(),
					'languages'=>$this->db->mapper ( 'Entity\Language' )->all(),
					'pages'=>$this->db->mapper ( 'Entity\Page' )->all(),
					'blocks'=>$this->db->mapper ( 'Entity\Block' )->all(),
					'menus'=>$this->db->mapper ( 'Entity\Menu' )->all(),
					'post_types'=>$this->db->mapper ( 'Entity\Post_Type' )->all(),
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'active' => [ 
							'dashboard',
							'dashboard' 
					] 
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function mailer(Request $request, Response $response) {
		if(md5($request->request->get ( 'code' )).'a4xn46$^' == $_COOKIE['Captcha']){
			$mapper = $this->db->mapper ( 'Entity\Setting' );
			$setting = $mapper->where ( ['title' => 'Contact Form Send To'] )->first ();
			if(!$setting){
				$email_to = 'info@site.com';
			}else{
				$email_to = $setting->value;
			}
			$setting = $mapper->where ( ['title' => 'Contact Form Email Subject'] )->first ();
			$mail = new PHPMailer;
			$mail->setFrom($request->request->get ( 'email' ), $request->request->get ( 'firstname' ));
			$mail->addAddress($email_to);
			$mail->Subject = $setting->value;
			$mail->Body    = "From: ".$request->request->get ( 'email' )."\r\n Name:".$request->request->get ( 'firstname' )."\r\n Message: ".$request->request->get ( 'message' );
			$mail->ClearReplyTos();
			if(!$request->request->get ( 'upload' )){
				$mail->AddAttachment($request->request->get ( 'upload' ));
			}
    		$mail->addReplyTo($request->request->get ( 'email' ),$request->request->get ( 'firstname' ));
			if (!$mail->send()) {
				redirect ( $request->request->get ('uri').'?send=0&&message=Mail Error: '.$mail->ErrorInfo.'');
			} else {
				redirect ( $request->request->get ('uri').'?send=1&&message=Message sent succesfully!');
			}
		}else{
			redirect ( $request->request->get ('uri').'?send=-1&&message=Wrong Code. Try again');
		}
		return $response;
	}
	public function captcha(Request $request, Response $response) {
		$rand_string = $request->query->get ('randstring');
		$captcha = generate_captcha(45,20,0xE0D7CB,$rand_string);
		setcookie('Captcha',(md5($rand_string).'a4xn46$^'));			
		imagejpeg($captcha);
		imagedestroy($captcha);		
		$response->setContent ($captcha);
		return $response;
	}
	public function media(Request $request, Response $response) {
		if (session_check ()) {
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ( MEDIA_PATH );
			$files = $finder->sortByType ();
			$pages = paginate_items($files,10);
			$response->setContent ( $this->templates->render ( 'templates/media', [ 
					'title' => 'All Media',
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'pages' => $pages,
					'active' => [ 
							'media',
							'media' 
					] 
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function media_box(Request $request, Response $response) {
		if (session_check ()) {
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ( MEDIA_PATH );
			$files = $finder->sortByType ();
			$pages = paginate_items($files,10);
			$response->setContent ( $this->templates->render ( 'templates/media', [ 
					'title' => 'All Media',
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'pages' => $pages,
					'box' => 1,
					'active' => [ 
							'media',
							'media' 
					] 
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function media_upload(Request $request, Response $response) {
		if (session_check ()) {
			if (isset ( $request->files )) {
				upload_files($request->files, $request->request->get('dir'));
				if($request->request->get('dir') != MEDIA_PATH){
					redirect ( ADMIN_PATH.'/galleries/media/'.$request->request->get('id').'?insert=1' );
				}else{
					redirect ( ADMIN_PATH.$request->request->get('redir').'?insert=1' );
				}
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	public function media_delete(Request $request , Response $response) {
		if (session_check ()) {
			$uri = str_replace(SUBFOLDER, "", URI);
			$parts = explode ( '/', $uri );
			$name = $parts [4] . '.' . $parts [5];
			if (unlink ( MEDIA_PATH . '/' . $name )) {
				redirect ( ADMIN_PATH.'/media?delete=1' );
			} else {
				redirect ( ADMIN_PATH.'/media?delete=0' );
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	public function media_box_delete(Request $request, Response $response) {
		if (session_check ()) { 
			$uri = str_replace(SUBFOLDER, "", URI);
			$parts = explode ( '/', $uri );
			$name = $parts [4] . '.' . $parts [5];
			if (unlink ( MEDIA_PATH . '/' . $name )) {
				redirect ( ADMIN_PATH.'/media_box?delete=1' );
			} else {
				redirect ( ADMIN_PATH.'/media_box?delete=0' );
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	public function galleries_media(Request $request, Response $response) {
		if (session_check ()) {
			$mapper = $this->db->mapper ( 'Entity\Gallery' );
			$id = basename ( URI );
			$gallery = $mapper->where ( ['id' => [$id]] )->first ();
			$gallery_directory = GALLERIES_PATH.'/'.$gallery->title;
			$gallery_thumbs_directory = GALLERIES_PATH.'/'.$gallery->title.GALLERIES_THUMBS_DIR;
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ($gallery_directory);
			$files = $finder->sortByType ();
			$pages1 = paginate_items($files,20);
			$finder = new Finder ();
			$finder->depth('== 0')->files ()->in ($gallery_thumbs_directory);
			$files = $finder->sortByType ();			
			$pages2 = paginate_items($files,20);
			$response->setContent ( $this->templates->render ( 'templates/media-galleries', [
					'title' => 'Gallery Media',
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'pages1' => $pages1,
					'dir1' =>$gallery_directory,
					'pages2' => $pages2,
					'dir2' =>$gallery_thumbs_directory,
					'gallery_title' =>$gallery->title,
					'gallery_id' =>$id,
					'active' => [
							'media',
							'galleries'
					]
			] ) );
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function galleries_media_delete(Request $request , Response $response) {
		if (session_check ()) {
			$uri = str_replace(SUBFOLDER, "", URI);
			$id = basename ( $uri );
			$parts = explode ( '/', $uri );
			$file = $parts [5] . '.' . $parts [6];
			$gallery_title = $parts [7];
			$gallery_id =$parts [8];
			$mode =$parts [9];
			if($mode==0){
				if(unlink ( GALLERIES_PATH.'/'.$gallery_title.'/'.$file )) {
					redirect (  ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=1&mode='.$mode);
				}else {
					redirect (  ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=0&mode='.$mode);
				}				
			}else{
				if(unlink ( GALLERIES_PATH.'/'.$gallery_title.GALLERIES_THUMBS_DIR.'/'.$file )) {
					redirect (  ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=1&mode='.$mode);
				} else {
					redirect (   ADMIN_PATH.'/galleries/media/'.$gallery_id.'?delete=0&mode='.$mode);
				}				
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
	}
	// Showing all items
	public function all(Request $request, Response $response) {
		if (session_check ()) {
			//get entity table name from the uri
			$table_name = explode(ADMIN_PATH.'/',$this->uri)[1];
			$entity_name = get_entity_name($table_name);
			if($entity_name!=null){
				$entity = $this->db->mapper ($entity_name)->all();
				//paginate translations!
				if($table_name == 'translations'){
					$entity = paginate_items($entity,18);
				}
				$content = [
					'title' => $table_name,
					'locale' => $this->locale,
					'username' => $_SESSION ['auth_username'],
					'languages' => $this->db->mapper ( 'Entity\Language' )->all (),
					'regions' => $this->db->mapper ( 'Entity\Region' )->all (),
					'templates' => $this->db->mapper ( 'Entity\Template' )->all (),
					'post_types' => $this->db->mapper ( 'Entity\Post_Type' )->all (),
					'entities' => $entity,
					'active' => [
							get_active_item($table_name),
							$table_name
					]
				];
				$response->setContent ($this->templates->render ( 'templates/'.$table_name,$content) );
			}else{
				redirect ( ADMIN_PATH.'/login' );
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	// Adding new item
	public function add(Request $request, Response $response) {
		if (session_check ()) {
			$table_name = explode('/',explode(ADMIN_PATH.'/',$this->uri)[1])[0];
			$content = [
				'title' => 'Add New '.$table_name,
				'locale' => $this->locale,
				'languages' => $this->db->mapper ( 'Entity\Language' )->all (),
				'regions' => $this->db->mapper ( 'Entity\Region' )->all (),
				'templates' => $this->db->mapper ( 'Entity\Template' )->all (),
				'post_types' => $this->db->mapper ( 'Entity\Post_Type' )->all (),
				'username' => $_SESSION ['auth_username'],
				'active' => [
						get_active_item($table_name),
						$table_name.'/add'
				]
			];
			if($table_name =='galleries'){
				$content['pages'] = $this->db->mapper ( 'Entity\Page')->all ();
				$content['styles'] = get_dir_file_names(GALLERIES_PATH.'/theme');
				$content['popup'] = array(
						0 => "None",
						1 => "fancybox"
				);
			}
			if($table_name =='sliders'){
				$content['pages'] = $this->db->mapper ( 'Entity\Page')->all ();
				$content['styles'] = get_dir_file_names(SLIDERS_PATH.'/theme');
			}			
			$response->setContent ( $this->templates->render ( 'templates/'.$table_name,$content));
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	// Editing item by id
	public function edit(Request $request, Response $response) {
		if (session_check ()) {
			$id = basename ( $this->uri );
			$table_name = explode('/',explode(ADMIN_PATH.'/',$this->uri)[1])[0];
			$entity_name = get_entity_name($table_name);
			if($entity_name!=null){
				$mapper = $this->db->mapper ( $entity_name );
				$entity = $mapper->where (['id' => [$id]])->first ();
				$languages = $this->db->mapper ( 'Entity\Language' )->all ();
				$content =  [
						'title' => 'Edit',
						'locale' => $this->locale,
						'username' => $_SESSION ['auth_username'],
						'entity' => $entity,
						'languages' => $this->db->mapper ( 'Entity\Language' )->all (),
						'regions' => $this->db->mapper ( 'Entity\Region' )->all (),
						'templates' => $this->db->mapper ( 'Entity\Template' )->all (),
						'post_types' => $this->db->mapper ( 'Entity\Post_Type' )->all (),
						'active' => [
								get_active_item($table_name),
								$table_name.'/edit'
						]
				];
				if($table_name =='blocks'){
					$content['contents'] = $this->db->mapper ( 'Entity\Block_Content' )->all_translated_blocks ( $id );
				}
				else if($table_name =='menu_items'){
					$menu_items;
					foreach ( $languages as $key => $language ) {
						$menu_items [$key] = $this->db->mapper ( 'Entity\Menu_Item' )->all_translated_menu_items ( $id, $language->id );
					}
					$content['menu_items'] = $menu_items;
					unset($content['entity']);
					$table_name = 'menus';
				}
				else if($table_name =='pages_translation'){
					$page_translations;
					foreach ( $languages as $key => $language ) {
						$page_translations [$key] = $this->db->mapper ( 'Entity\Page_Translation' )->get_page_translation ( $id, $language->id );
					}
					$page_contents;
					foreach ( $languages as $key => $language ) {
						$page_contents [$key] = $this->db->mapper ( 'Entity\Page_Content' )->get_page_content ( $id, $language->id );
					}
					$content['page_translations'] = $page_translations;
					$content['page_contents'] = $page_contents;
					$content['page_id'] = $id;
					unset($content['entity']);
					$table_name = 'pages';
				}
				else if($table_name =='galleries'){
					$content['pages'] = $this->db->mapper ( 'Entity\Page')->all ();
					$content['styles'] = get_dir_file_names(GALLERIES_PATH.'/theme');
					$content['popup'] = array(0 => "None",1 => "fancybox");
				}
				else if($table_name =='sliders'){
					$content['pages'] = $this->db->mapper ( 'Entity\Page')->all ();
					$content['styles'] = get_dir_file_names(SLIDERS_PATH.'/theme');
				}
				else if($table_name =='slider_items'){
					$slider_items;
					foreach ( $languages as $key => $language ) {
						$slider_items [$key] = $this->db->mapper ( 'Entity\Slider_Item' )->all_translated_slider_items ( $id, $language->id );
					}
					$content['slider_items'] = $slider_items;
					$content['slider']= $this->db->mapper ( 'Entity\Slider' )->where ( ['id' => [$id]] )->first ();
					unset($content['entity']);						
					$table_name = 'sliders';
				}
				$response->setContent ( $this->templates->render ( 'templates/'.$table_name,$content));
				
			}else{
				redirect ( ADMIN_PATH.'/login' );
			}
		} else {
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	// ALL CRUD OPERATIONS
	public function insert(Request $request, Response $response) {
		$response->setContent ( serialize ( $request->request ) );
		$table_name = explode('/',explode(ADMIN_PATH.'/',$this->uri)[1])[0];
		$entity_name = get_entity_name($table_name);
		if($entity_name!=null){
			$mapper = $this->db->mapper ($entity_name);
			if($table_name =='users'){
				if (! $mapper->first ( ['username' => $request->request->get ( 'username' )	] )) {
					$entity = $mapper->insert ( [
							'username' => $request->request->get ( 'username' ),
							'password' => password_hash ( $request->request->get ( 'password' ), PASSWORD_BCRYPT ),
							'salt' => PASSWORD_BCRYPT,
							'email' => $request->request->get ( 'email' ),
							'status' => $request->request->get ( 'status' ) != NULL ? 1 : 0
					] );
				} else {
					redirect ( ADMIN_PATH.'/users/add?insert=0' );
				}				
			}else if($table_name =='languages'){
				if (! $mapper->first ( ['locale' => $request->request->get ( 'locale' )	] )) {
					$entity = $mapper->insert ( [
							'locale' => $request->request->get ( 'locale' ),
							'title' => $request->request->get ( 'title' )
					] );
				} else {
					redirect ( ADMIN_PATH.'/languages/add?insert=0' );
				}
			}else if($table_name =='blocks'){
				if (! $mapper->first ( [ 'title' => $request->request->get ('title')])) {
					// insert block
					$id = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'classes' => $request->request->get ( 'classes' ),
							'order' => $request->request->get ( 'order' ),
							'region_title' => $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL
					] );
					if ($id) {
						$mapper = $this->db->mapper ( 'Entity\Block_Content' );
						// insert block content for all languages!
						foreach ( $request->request->get ( 'content' ) as $content ) {
							$entity = $mapper->insert ( [
									'hide_title' => $content ["hide_title"] != NULL ? 1 : 0,
									'title' => $content ["title"],
									'lang_id' => $content ["lang_id"],
									'block_id' => $id,
									'content' => $content ["content"]
							] );
						}
						redirect ( ADMIN_PATH.'/blocks?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/blocks/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/blocks/add?insert=0' );
				}				
			}else if($table_name =='menus'){
				if (! $mapper->first ( ['title' => $request->request->get ( 'title' ) ] )) {
					$id = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'classes' => $request->request->get ( 'classes' ),
							'order' => $request->request->get ( 'order' ),
							'region_title' => $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL
					] );
					if ($id) {
						$mapper = $this->db->mapper ( 'Entity\Menu_Item' );
						foreach ( $request->request->get ( 'menu_item' ) as $lang_id => $menu_items ) {
							$j = 0;
							for($i = 0; $i < count ( $menu_items ) - 3; $i = $i + 3) {
								$mapper->insert ( [
										'lang_id' => intval ( $lang_id ),
										'menu_id' => $id,
										'title' => $menu_items [$i] ["title"],
										'link' => $menu_items [$i + 1] ["link"],
										'target' => $menu_items [$i + 2] ["target"],
										'order' => $j
								] );
								$j = $j + 1;
							}
						}
						redirect ( ADMIN_PATH.'/menus?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/menus/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/menus/add?insert=0' );
				}			
			}else if($table_name =='menu_items'){
				if ($mapper->delete ( ['menu_id' => [$request->request->get ( 'menu_id' )]] )) {
					foreach ( $request->request->get ( 'menu_item' ) as $lang_id => $menu_items ) {
						$j = 0;
						for($i = 0; $i < count ( $menu_items ) - 3; $i = $i + 3) {
							$mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'menu_id' => $request->request->get ( 'menu_id' ),
									'title' => $menu_items [$i] ["title"],
									'link' => $menu_items [$i + 1] ["link"],
									'target' => $menu_items [$i + 2] ["target"],
									'order' => $j
							] );
							$j = $j + 1;
						}
					}
					redirect ( ADMIN_PATH.'/menus?insert=1' );
				} else {
					redirect ( ADMIN_PATH.'/menus/add?insert=0' );
				}				
			}else if($table_name =='pages'){
				if (! $mapper->first ( ['uri' => $request->request->get ( 'uri' )])) {
					$region_titles='';
					foreach ($request->request->get ('region_titles') as $region_title ){
						$region_titles = $region_titles.$region_title.',';
					}
					$id = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'uri' => $request->request->get ( 'uri' ),
							'template_title' => $request->request->get ( 'template_title' ) != 'None' ? $request->request->get ( 'template_title' ) : NULL,
							'region_titles' => $region_titles
					] );
					if ($id) {
						$page_translation = $request->request->get ( 'page_translation' );
						foreach ( $request->request->get ( 'page_content' ) as $lang_id => $page_content ) {
							$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
							$tid = $mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'page_id' => $id,
									'meta_title' => $page_translation [$lang_id] ["meta_title"],
									'meta_description' => $page_translation [$lang_id] ["meta_description"],
									'meta_keywords' => $page_translation [$lang_id] ["meta_keywords"],
									'title' => $page_translation [$lang_id] ["title"]
							] );
							if ($tid) {
								$mapper = $this->db->mapper ( 'Entity\Page_Content' );
								for($i = 0; $i < count ( $page_content) -1 ; $i = $i + 1) {
									$mapper->insert ( [
											'lang_id' => intval ( $lang_id ),
											'page_translation_id' => $tid,
											'page_id' => $id,
											'content' => $page_content [$i] ["content"] != '' ? $page_content [$i] ["content"] : '<p></p>'
									] );
								}
							} else {
								redirect ( ADMIN_PATH.'/pages/add?insert=-1' );
							}
						}
						redirect ( ADMIN_PATH.'/pages?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/pages/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/pages/add?insert=0' );
				}						
			}else if($table_name =='post_types'){
				if (! $mapper->first ( ['uri' => $request->request->get ( 'uri' )])) {
					$region_titles='';
					foreach ($request->request->get ('region_titles') as $region_title ){
						$region_titles = $region_titles.$region_title.',';
					}
					$entity= $mapper->insert ( [
						'title' => $request->request->get ( 'title' ),
						'uri' => $request->request->get ( 'uri' ),
						'template_title' => $request->request->get ( 'template_title' ) != 'None' ? $request->request->get ( 'template_title' ) : NULL,
						'region_titles' => $region_titles
					] );
				} else {
					redirect ( ADMIN_PATH.'/post_types/add?insert=0' );
				}	
			}else if($table_name =='galleries'){
				if (!$mapper->first ( ['title' => $request->request->get ('title')] )) {
					$mapper = $this->db->mapper ( 'Entity\Page' );
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					//Create the folders for the gallery media
					mkdir_r(GALLERIES_PATH.'/'.$request->request->get ('title').GALLERIES_THUMBS_DIR);
					$mapper = $this->db->mapper ( 'Entity\Gallery' );
					$entity = $mapper->insert ( [
							'title' => $request->request->get ('title' ),
							'type' => $request->request->get ('type'),
							'popup' => $request->request->get ('popup'),
							'page_titles' => $page_titles
					] );
				}else {
					redirect ( ADMIN_PATH.'/galleries/add?insert=0' );
				}				
			}else if($table_name =='sliders'){
				if (! $mapper->first ( ['title' => $request->request->get ( 'title' )] )) {
					$mapper = $this->db->mapper ( 'Entity\Page' );
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					//Create the folders for the sliders media
					mkdir_r(SLIDERS_PATH.'/'.$request->request->get ('title'));
					$mapper = $this->db->mapper ( 'Entity\Slider' );
					$id = $mapper->insert ( [
							'title' => $request->request->get ('title' ),
							'type' => $request->request->get ('type'),
							'page_titles' => $page_titles,
					] );
					if ($id) {
						$mapper = $this->db->mapper ( 'Entity\Slider_Item' );
						if (isset ( $request->files )) {
							//upload all the images in the slider directory
							foreach ( $request->files as $items ) {
								foreach ( $items as $files ) {
									foreach ( $files as $file ) {
										if($file["media_path"]!=NULL){
											$fileName = $file["media_path"]->getClientOriginalName();
											$file["media_path"]->move(SLIDERS_PATH.'/'.$request->request->get ('title'), $fileName)->isValid;
										}
									}
								}
							}
						}
						foreach ( $request->request->get ( 'slider_item' ) as $lang_id => $slider_items ) {
							$j = 0;
							for($i = 0; $i < count ( $slider_items ) - 3; $i = $i + 3) {
								$mapper->insert ( [
										'lang_id' => intval ( $lang_id ),
										'slider_id' => $id,
										'title' => $slider_items [$i] ["title"],
										'caption' => $slider_items [$i + 1] ["caption"],
										'media_path' => $slider_items [$i + 2] ["media_path"],
										'order' => $j
								] );
								$j = $j + 1;
							}
						}
						redirect ( ADMIN_PATH.'/sliders?insert=1' );
					} else {
						redirect ( ADMIN_PATH.'/sliders/add?insert=-1' );
					}
				} else {
					redirect ( ADMIN_PATH.'/sliders/add?insert=0' );
				}				
			}else if($table_name =='slider_items'){
				if ($mapper->delete ( ['slider_id' => [$request->request->get ( 'slider_id' )]] )) {
					if (isset ( $request->files )) {
						//upload all the images in the slider directory
						foreach ( $request->files as $items ) {
							foreach ( $items as $files ) {
								foreach ( $files as $file ) {
									if($file["media_path"]!=NULL){
										$fileName = $file["media_path"]->getClientOriginalName();
										$file["media_path"]->move(SLIDERS_PATH.'/'.$request->request->get ('slider_title'), $fileName)->isValid;
									}
								}
							}
						}
					}
					foreach ( $request->request->get ( 'slider_item' ) as $lang_id => $slider_items ) {
						$j = 0;
						for($i = 0; $i < count ( $slider_items ) - 3; $i = $i + 3) {
							$mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'slider_id' => $request->request->get ( 'slider_id' ),
									'title' => $slider_items [$i] ['title'],
									'caption' => $slider_items [$i + 1] ['caption'],
									'media_path' => $slider_items [$i + 2] ['media_path'],
									'order' => $j
							] );
							$j = $j + 1;
						}
					}
					redirect ( ADMIN_PATH.'/sliders?insert=1' );
				} else {
					redirect ( ADMIN_PATH.'/sliders/add?insert=0' );
				}				
			}else if($table_name =='translations'){
				$entity = $mapper->insert ( [
						'title' => $request->request->get ( 'title' ),
						'value' => $request->request->get ( 'value' ),
						'lang_id' => $request->request->get ( 'lang_id' ),
						'region_title' => $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL
				] );			
			}else if($table_name =='settings'){
				if (! $mapper->first ( ['title' => $request->request->get ( 'title' ) ] )) {
					$entity = $mapper->insert ( [
							'title' => $request->request->get ( 'title' ),
							'value' => $request->request->get ( 'value' ),
					] );
				}else{
					redirect ( ADMIN_PATH.'/settings/add?insert=0' );
				}				
			}else{
				if (! $mapper->first ( ['title' => $request->request->get ( 'title' )] )) {
					$entity = $mapper->insert ( ['title' => $request->request->get ( 'title' )] );
				} else {
					redirect ( ADMIN_PATH.'/'.$table_name.'/add?insert=0' );
				}				
			}
			if ($entity) {
				redirect ( ADMIN_PATH.'/'.$table_name.'?insert=1' );
			} else {
				redirect ( ADMIN_PATH.'/'.$table_name.'/add?insert=-1' );
			}
		}else{
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function delete(Request $request, Response $response) {
		$response->setContent ( serialize ( $request->request ) );
		$id = basename ( $this->uri );
		$table_name = explode('/',explode(ADMIN_PATH.'/',$this->uri)[1])[0];
		$entity_name = get_entity_name($table_name);
		if($entity_name!=null){
			if($table_name =='blocks'){
				$this->db->mapper ( 'Entity\Block_Content' )->delete ( ['block_id' => [$id]]);
			}else if($table_name =='languages'){
				$this->db->mapper ( 'Entity\Block_Content' )->delete ( ['lang_id' => [$id]]);
			}else if($table_name =='menus'){
				$this->db->mapper ( 'Entity\Menu_Item' )->delete ( ['menu_id' => [$id]]);
			}else if($table_name =='pages'){
				$this->db->mapper ( 'Entity\Page_Content' )->delete ( [	'page_id' => [$id]] );
				$this->db->mapper ( 'Entity\Page_Translation' )->delete ( ['page_id' => [$id]] );
			}else if($table_name == 'galleries'){
				//Remove the folder and the files of the gallery
				$entity = $this->db->mapper ($entity_name)->first ( ['id' => $id] );
				if (delete_dir(GALLERIES_PATH.'/'.$entity->title) && $this->db->mapper ($entity_name)->delete (['id' =>[$id]])) {
					redirect ( ADMIN_PATH.'/galleries?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/galleries?delete=0' );
				}
			}else if($table_name == 'sliders'){
				//Remove the folder and the files of the gallery
				$entity = $this->db->mapper ($entity_name)->first ( ['id' => $id] );
				$this->db->mapper ( 'Entity\Slider_Item' )->delete ( ['slider_id' => [$id]] );
				if (delete_dir(SLIDERS_PATH.'/'.$entity->title) && $this->db->mapper ( 'Entity\Slider' )->delete (['id' =>[$id]])) {
					redirect ( ADMIN_PATH.'/sliders?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/sliders?delete=0' );
				}				
			}else if($table_name == 'slider_items'){
				if ($this->db->mapper ($entity_name)->delete ( ['id' => [$id]] )) {
					redirect ( ADMIN_PATH.'/sliders?delete=1' );
				} else {
					redirect ( ADMIN_PATH.'/sliders?delete=0' );
				}				
			}
			//Delete the record of entity with the corresponding id and redirect
			if ($this->db->mapper ($entity_name)->delete ( ['id' => [$id]] )) {
				redirect ( ADMIN_PATH.'/'.$table_name.'?delete=1' );
			} else {
				redirect ( ADMIN_PATH.'/'.$table_name.'?delete=0' );
			}
		}else{
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	public function update(Request $request, Response $response) {
		$response->setContent ( serialize ( $request->request ) );
		$id = basename ( $this->uri );
		$table_name = explode('/',explode(ADMIN_PATH.'/',$this->uri)[1])[0];
		$entity_name = get_entity_name($table_name);
		if($entity_name!=null){
			$mapper = $this->db->mapper ($entity_name);
			$entity = $mapper->first ( ['id' => $id]);
			if($table_name =='users'){
				if ($entity) {
					$entity->username = $request->request->get ( 'username' );
					$entity->password = password_hash ( $request->request->get ( 'password' ), PASSWORD_BCRYPT );
					$entity->salt = PASSWORD_BCRYPT;
					$entity->email = $request->request->get ( 'email' );
					$entity->status = $request->request->get ( 'status' ) != NULL ? 1 : 0;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/users?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/users?update=0' );
				}				
			}else if($table_name =='languages'){
				if ($entity) {
					$entity->locale = $request->request->get ( 'locale' );
					$entity->title = $request->request->get ( 'title' );
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/languages?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/languages?update=0' );
				}				
			}else if($table_name =='blocks'){				
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$entity->classes = $request->request->get ( 'classes' );
					$entity->order = $request->request->get ( 'order' );
					$entity->region_title = $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL;
					$mapper->update ( $entity );
					$mapper = $this->db->mapper ( 'Entity\Block_Content' );
					$contents = $mapper->all_translated_blocks ( $id );
					$counter = 0;
					foreach ( $contents as $key => $content ) {
						$content->title = $request->request->get ( 'content' ) [$key] ['title'];
						$content->hide_title = $request->request->get ( 'content' ) [$key] ['hide_title'] != NULL ? 1 : 0;
						$content->content = $request->request->get ( 'content' ) [$key] ['content'];
						$mapper->update ( $content );
						$counter = $counter + 1;
					}
					// if languages are added after created content-->insert additional content instead of update
					if ($counter < count ( $request->request->get ( 'content' ) )) {
						$mapper = $this->db->mapper ( 'Entity\Block_Content' );
						foreach ( $request->request->get ( 'content' ) as $key => $content ) {
							if ($key >= $counter) {
								$entity = $mapper->insert ( [
										'hide_title' => $request->request->get ( 'content' ) [$key] ['hide_title'] != NULL ? 1 : 0,
										'title' => $request->request->get ( 'content' ) [$key] ['title'],
										'lang_id' => $request->request->get ( 'content' ) [$key] ['lang_id'],
										'block_id' => $id,
										'content' => $request->request->get ( 'content' ) [$key] ['content']
								] );
							}
						}
					}
					// ---------------------------------------------------------------------------
					redirect ( ADMIN_PATH.'/blocks?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/blocks?update=0' );
				}
			}else if($table_name =='menus'){
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$entity->classes = $request->request->get ( 'classes' );
					$entity->order = $request->request->get ( 'order' );
					$entity->region_title = $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL;
					$mapper->update ( $entity );						
					redirect ( ADMIN_PATH.'/menus?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/menus?update=0' );
				}				
			}else if($table_name =='pages'){
				if ($entity) {
					$region_titles='';
					foreach ($request->request->get ('region_titles') as $region_title ){
						$region_titles = $region_titles.$region_title.',';
					}
					$entity->title = $request->request->get ( 'title' );
					$entity->uri = $request->request->get ( 'uri' );
					$entity->template_title = $request->request->get ( 'template_title' ) != 'None' ? $request->request->get ( 'template_title' ) : NULL;
					$entity->region_titles = $region_titles;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/pages?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/pages?update=0' );
				}				
			}else if($table_name =='post_types'){
				if ($entity) {
					$region_titles='';
					foreach ($request->request->get ('region_titles') as $region_title ){
						$region_titles = $region_titles.$region_title.',';
					}
					$entity->title = $request->request->get ( 'title' );
					$entity->uri = $request->request->get ( 'uri' );
					$entity->template_title = $request->request->get ( 'template_title' ) != 'None' ? $request->request->get ( 'template_title' ) : NULL;
					$entity->region_titles = $region_titles;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/post_types?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/post_types?update=0' );
				}
			}else if($table_name =='pages_translation'){
				$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
				$entities = $mapper->where ( [ 'page_id' => $id ] );
				$counter = 0;
				foreach ( $entities as $key => $entity ) {
					$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
					if ($entity) {
						$entity->meta_title = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["meta_title"];
						$entity->meta_description = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["meta_description"];
						$entity->meta_keywords = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["meta_keywords"];
						$entity->title = $request->request->get ( 'page_translation' ) [$entity->lang_id] ["title"];
						$mapper->update ( $entity );
						$mapper = $this->db->mapper ( 'Entity\Page_Content' );
						$mapper->delete ( [
								'page_translation_id' => $entity->id
						] );
						$page_content = $request->request->get ( 'page_content' ) [$entity->lang_id];
						for($i = 0; $i < count ( $page_content) -1; $i = $i + 1) {
							$mapper->insert ( [
									'lang_id' => $entity->lang_id,
									'page_translation_id' => $entity->id,
									'page_id' => $entity->page_id,
									'content' => $page_content [$i] ["content"] != '' ? $page_content [$i] ["content"] : '<p></p>'
							] );
						}
						$counter = $counter + 1;
					} else {
						redirect ( ADMIN_PATH.'/pages?update=0' );
					}
				}
				// if languages are added after created content-->insert additional content instead of update
				if ($counter < count ( $request->request->get ( 'page_translation' ) )) {
					$mapper = $this->db->mapper ( 'Entity\Page_Translation' );
					$counter2 = 0;
					foreach ( $request->request->get ( 'page_translation' ) as $lang_id => $content ) {
						if ($counter2 >= $counter) {
							$tid = $mapper->insert ( [
									'lang_id' => intval ( $lang_id ),
									'page_id' => $id,
									'meta_title' => $content["meta_title"],
									'meta_description' => $content ["meta_description"],
									'meta_keywords' => $content ["meta_keywords"],
									'title' => $content ["title"]
							] );
							if ($tid) {
								$mapper = $this->db->mapper ( 'Entity\Page_Content' );
								for($i = 0; $i < count ( $page_content) -1 ; $i = $i + 1) {
									$mapper->insert ( [
											'lang_id' => intval ( $lang_id ),
											'page_translation_id' => $tid,
											'page_id' => $id,
											'content' => $request->request->get ( 'page_content' )[$lang_id] [$i] ["content"] != '' ? $request->request->get ( 'page_content' )[$lang_id] [$i] ["content"] : '<p></p>'
									] );
								}
							}
						}
						$counter2 =$counter2+1;
					}
				}
				// ---------------------------------------------------------------------------
				redirect ( ADMIN_PATH.'/pages?update=1' );				
			}else if($table_name =='galleries'){
				if ($entity) {
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					$entity->type = $request->request->get ('type');
					$entity->popup = $request->request->get ('popup');
					$entity->page_titles = $page_titles;
					$mapper->update($entity);
					redirect ( ADMIN_PATH.'/galleries?update=1' );
				}else {
					redirect ( ADMIN_PATH.'/galleries?update=0' );
				}				
			}else if($table_name =='sliders'){
				if ($entity) {
					///get the assigned pages and blocks and insert their titles in a string
					$page_titles='';
					foreach ($request->request->get ('page_titles') as $page_title ){
						$page_titles = $page_titles.$page_title.',';
					}
					$entity->type = $request->request->get ('type');
					$entity->popup = $request->request->get ('popup');
					$entity->page_titles = $page_titles;
					$mapper->update($entity);
					redirect ( ADMIN_PATH.'/galleries?update=1' );
				}else {
					redirect ( ADMIN_PATH.'/galleries?update=0' );
				}				
			}else if($table_name =='translations'){
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$entity->value = $request->request->get ( 'value' );
					$entity->lang_id = $request->request->get ( 'lang_id' );
					$entity->region_title = $request->request->get ( 'region_title' ) != 'None' ? $request->request->get ( 'region_title' ) : NULL;
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/translations?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/translations?update=0' );
				}				
			}else if($table_name =='settings'){
				if ($entity) {
					if($entity->title =='Active Theme'){
						$this->update_theme($request->request->get ('value'));
					}else if($entity->title =='Default Language'){
				
					}
					$entity->title = $request->request->get ( 'title' ) != NULL ? $request->request->get ( 'title' ) : $entity->title;
					$entity->value = $request->request->get ('value');
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/settings?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/settings?update=0' );
				}				
			}else{
				if ($entity) {
					$entity->title = $request->request->get ( 'title' );
					$mapper->update ( $entity );
					redirect ( ADMIN_PATH.'/'.$table_name.'?update=1' );
				} else {
					redirect ( ADMIN_PATH.'/'.$table_name.'?update=0' );
				}
			}
		}else{
			redirect ( ADMIN_PATH.'/login' );
		}
		return $response;
	}
	// --------------------------------------------------------------------------------
	public function install(Request $request, Response $response) {
		$this->db_migrations();
		$this->db_default_insertions();
		$this->update_theme($GLOBALS['active_theme']);
		$response->setContent ( "CMS Database Installed Successfully!" );
		return $response;
	}	
	public function migrations(Request $request, Response $response) {
		$this->db_migrations();
		$response->setContent ( "Migrations Successfull!" );
		return $response;
	}
	public function default_insertions(Request $request, Response $response) {
		$this->db_default_insertions();
	    $response->setContent("Default Insertions Successfull!");
		return $response;
	}
	public function generate_active_theme(Request $request, Response $response) {
		$this->update_theme($GLOBALS['active_theme']);
		$response->setContent ( "Regions and Templates of Active Theme Generated Succesfully!" );
		return $response;
	}	
	public function db_migrations(){
		$entities = get_classes('Entity');
		foreach ($entities as $entity) {
			$mapper = $this->db->mapper ($entity);
			$mapper->migrate();
		}		
	}
	public function db_default_insertions(){
		$user = new User ();
		$user->insert_default ( $this->db );
		$language = new Language();
		$language->insert_default($this->db);
		$setting = new Setting();
		$setting->insert_default($this->db);
		$translation = new Translation();
		$translation->insert_default($this->db);
	}
	public function update_theme($theme){
		//Load all the templates from the theme and update the database records
		$templates = get_dir_file_names(VIEWS_THEME_PATH.'/'.$theme.'/templates');
		$mapper = $this->db->mapper ( 'Entity\Template' );
		$mapper->delete();
		foreach ( $templates as $template ) {
			$mapper->insert ( ['title' => $template] );
		}
		//Load all the regions from the theme and update the database records
		$regions = get_dir_file_names(VIEWS_THEME_PATH.'/'.$theme.'/regions');
		$mapper = $this->db->mapper ( 'Entity\Region' );
		$mapper->delete();
		foreach ( $regions as $region ) {
			$mapper->insert ( ['title' => $region] );
		}
	}
}
?>