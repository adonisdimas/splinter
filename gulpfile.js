var gulp = require('gulp');
var browserSync = require('browser-sync');
var php = require('gulp-connect-php');
var reload  = browserSync.reload;
var phpDir = 'source';
	
gulp.task('webserver', function() {
    php.server({ base: phpDir, port: 8014, keepalive: true});
});

gulp.task('browser-sync-web', function() {
    browserSync({
        proxy: '127.0.0.1:8014',
        port: 8084,
        open: true,
        notify: false
    });
});
/*watch dynamic_templates task*/
gulp.task('watch', ['webserver','browser-sync-web'], function (){
	gulp.watch(phpDir+'/**/*.php').on("change", reload);
});